﻿using Shapes.GeometryElements;

namespace Shapes.Figures
{
    internal class Rectangle : RotatableShape
    {
        public Rectangle() : base(1)
        {
        }

        public Rectangle(Point leftTop, Size size, int rotationCoefficient) : base(rotationCoefficient)
        {
            Points.Add(leftTop);
            Points.Add(new Point(leftTop.X + size.Width, leftTop.Y));
            Points.Add(new Point(leftTop.X + size.Width, leftTop.Y + size.Width));
            Points.Add(new Point(leftTop.X, leftTop.Y + size.Height));
        }
    }
}