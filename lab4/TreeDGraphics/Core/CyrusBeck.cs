﻿using System;
using TreeDGraphics.Core.TwoD;
using TreeDGraphics.Helpers;

namespace TreeDGraphics.Core
{
    public class CyrusBeck
    {
        public static float CrossPoint(Line2D crossingLine, Line2D edgeLine)
        {
            var edgeNormalVect = edgeLine.NormalVect();

            var t = -1 * edgeNormalVect.ScalarMul(crossingLine.Start.VectSub(edgeLine.Start)) /
                    edgeNormalVect.ScalarMul(crossingLine.DirectVect());

            if (float.IsNaN(t))
                return 0;

            return t;
        }

        public static (float, float) CrossByLine(Polygon2D primaryPolygon, Line2D line, bool isInside = true)
        {
            if (!primaryPolygon.IsOrdered)
                throw new Exception("Cyrus-Beck algorithm works only with ordered poligons!");

            float tStart = 0;
            float tEnd = 1;
            var isJoining = true;

            foreach (var edge in primaryPolygon.Lines)
            {
                switch (Math.Sign(line.DirectVect().ScalarMul(edge.NormalVect())))
                {
                    case -1:
                        tStart = Math.Max(tStart, CrossPoint(line, edge));
                        break;
                    case +1:
                        tEnd = Math.Min(tEnd, CrossPoint(line, edge));
                        break;
                    case 0:
                        if (edge.IsLeft(line))
                            isJoining = false;
                        break;
                }

                if (!isJoining) break;
            }

            if (!isInside && (!isJoining || tStart > tEnd || tStart >= 1 || tEnd <= 0))
            {
                return (0, 1);
            }

            if (isJoining && tStart < tEnd)
                return (tStart, tEnd);

            return (0, 0);
        }
    }
}
