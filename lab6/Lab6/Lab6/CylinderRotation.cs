﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab6
{
    public class CylinderRotation
    {
        public bool IsEnabled { get; private set; }
        public double RotationX { get; private set; }
        public double RotationY { get; private set; }
        public double RotationZ { get; private set; }
        public double Angle { get; private set; }

        private readonly int Coefficient = 3;
        private readonly int AngleCoefficient = 1;

        private Dictionary<Keys, Action<CylinderRotation>> actions = new Dictionary<Keys, Action<CylinderRotation>>
        {
            { Keys.Q, (cylinderRotation) => { cylinderRotation.ChangeAngle(1); } },
            { Keys.W, (cylinderRotation) => { cylinderRotation.ChangeRotationX(1); } },
            { Keys.S, (cylinderRotation) => { cylinderRotation.ChangeRotationX(-1); } },
            { Keys.E, (cylinderRotation) => { cylinderRotation.ChangeRotationY(1); } },
            { Keys.D, (cylinderRotation) => { cylinderRotation.ChangeRotationY(-1); } },
            { Keys.R, (cylinderRotation) => { cylinderRotation.ChangeRotationZ(1); } },
            { Keys.F, (cylinderRotation) => { cylinderRotation.ChangeRotationZ(-1); } },
            { Keys.T, (cylinderRotation) => { cylinderRotation.ChangeAngle(-1); } },
        };

        public CylinderRotation(bool isEnabled)
        {
            RotationX = 0;
            RotationY = 0;
            RotationZ = 0;
            Angle = 0;
            IsEnabled = isEnabled;
        }

        public void OnKeyPressed(Keys keyChar)
        {
            if (!this.IsEnabled || !this.actions.ContainsKey(keyChar))
            {
                return;
            }

            this.actions[keyChar](this);
        }

        public void ChangeState()
        {
            this.IsEnabled = !this.IsEnabled;
        }

        private void ChangeAngle(int direction)
        {
            this.Angle += direction * AngleCoefficient;
        }

        private void ChangeRotationX(int direction)
        {
            this.RotationX += direction * Coefficient;
        }

        private void ChangeRotationY(int direction)
        {
            this.RotationY += direction * Coefficient;
        }

        private void ChangeRotationZ(int direction)
        {
            this.RotationZ += direction * Coefficient;
        }
    }
}
