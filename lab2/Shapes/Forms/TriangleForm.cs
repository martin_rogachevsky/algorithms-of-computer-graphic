﻿using Shapes.Figures;
using Shapes.GeometryElements;

namespace Shapes.Forms
{
    internal class TriangleForm : AbstractForm
    {
        protected override RotatableShape CreateShape()
        {
            return new Triangle(new Point(150, 100), 400, 1);
        }
    }
}