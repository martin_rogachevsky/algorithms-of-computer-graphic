﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab6
{
    public class SourceLightPosition
    {
        public bool IsEnabled { get; private set; }
        public float PositionX { get; private set; }
        public float PositionY { get; private set; }
        public float PositionZ { get; private set; }

        private readonly int Coefficient = 3;

        private Dictionary<Keys, Action<SourceLightPosition>> actions = new Dictionary<Keys, Action<SourceLightPosition>>
        {
            { Keys.W, (sourceLightPosition) => { sourceLightPosition.ChangePositionX(1); } },
            { Keys.S, (sourceLightPosition) => { sourceLightPosition.ChangePositionX(-1); } },
            { Keys.E, (sourceLightPosition) => { sourceLightPosition.ChangePositionY(1); } },
            { Keys.D, (sourceLightPosition) => { sourceLightPosition.ChangePositionY(-1); } },
            { Keys.R, (sourceLightPosition) => { sourceLightPosition.ChangePositionZ(1); } },
            { Keys.F, (sourceLightPosition) => { sourceLightPosition.ChangePositionZ(-1); } },
        };

        public SourceLightPosition(bool isEnabled)
        {
            PositionX = Coefficient;
            PositionY = Coefficient;
            PositionZ = Coefficient;
            IsEnabled = isEnabled;
        }

        public void OnKeyPressed(Keys keyChar)
        {
            if (!this.IsEnabled || !this.actions.ContainsKey(keyChar))
            {
                return;
            }

            this.actions[keyChar](this);
        }

        public void ChangeState()
        {
            this.IsEnabled = !this.IsEnabled;
        }

        private void ChangePositionX(int direction)
        {
            this.PositionX += direction * Coefficient;
        }

        private void ChangePositionY(int direction)
        {
            this.PositionY += direction * Coefficient;
        }

        private void ChangePositionZ(int direction)
        {
            this.PositionZ += direction * Coefficient;
        }
    }
}
