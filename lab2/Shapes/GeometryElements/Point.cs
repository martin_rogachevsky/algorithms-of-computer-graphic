﻿namespace Shapes.GeometryElements
{
    public class Point
    {
        public Point(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public Point()
        {
        }

        public double X { get; set; }

        public double Y { get; set; }
    }
}