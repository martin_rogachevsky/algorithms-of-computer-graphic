﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDL2;

namespace Lab1
{
    internal class ArchimedesSpiral
    {
        public IList<SDL.SDL_Point> points { get; private set; }

        private const int pointsCount = 1000000;

        private readonly double coefficient = 55.5;

        private readonly double initialAngleStep = 0.01;
        private readonly double initialSpiralStep = 1.5;

        private double parameter;

        private readonly double angleStep;
        private readonly double spiralStep;

        private int currentMoveStep { get; set; }
        private double currentRotateStep { get; set; }

        private readonly KeyboardOptions keyboardOptions;

        private SDL.SDL_Point currentPoint;
        private double currentAngle;

        public ArchimedesSpiral(int screenWidth, int screenHeight, double parameter, double? angleStep, double? spiralStep)
        {
            this.parameter = parameter;
            this.angleStep = angleStep ?? initialAngleStep;
            this.spiralStep = spiralStep ?? initialSpiralStep;
            this.keyboardOptions = new KeyboardOptions(1, 0.5, 0.02);
            this.currentMoveStep = this.keyboardOptions.MoveStep;
            this.currentRotateStep = this.keyboardOptions.InitialRotation;
            this.currentPoint = new SDL.SDL_Point
            {
                x = screenWidth / 2,
                y = screenHeight / 2
            };
            this.currentAngle = 0f;
            this.points = GeneratePoints(currentAngle, currentPoint, currentRotateStep);
        }

        public void OnMove(SDL.SDL_Keycode keycode)
        {
            currentMoveStep = keyboardOptions.OnMove(keycode);
            currentPoint.y += currentMoveStep;

            points = points.Select(it => new SDL.SDL_Point
            {
                x = it.x,
                y = it.y + currentMoveStep
            }).ToList();
        }

        public void OnRotate(SDL.SDL_Keycode keycode)
        {
            currentAngle += keyboardOptions.OnRotate(keycode);
            points = GeneratePoints(currentAngle, currentPoint, keyboardOptions.InitialRotation);
        }

        private IList<SDL.SDL_Point> GeneratePoints(double angle, SDL.SDL_Point centerPoint, double rotateStep)
        {
            var currentParameter = parameter;
            points = new List<SDL.SDL_Point>();
            for (var i = 0; i < pointsCount; i++)
            {
                points.Add(new SDL.SDL_Point()
                {
                    x = GetXPoint(currentParameter, angle) + centerPoint.x,
                    y = GetYPoint(currentParameter, angle) + centerPoint.y
                });
                angle += rotateStep;
                currentParameter += spiralStep;
            }
            return points;
        }

        private int GetXPoint(double parameter, double angle)
        {
            var result = parameter / (2 * Math.PI) * Math.Cos(angle);
            return (int)(result * coefficient);
        }

        private int GetYPoint(double parameter, double angle)
        {
            var result = parameter / (2 * Math.PI) * Math.Sin(angle);
            return (int)(result * coefficient);
        }
    }
}
