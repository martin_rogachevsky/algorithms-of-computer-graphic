using System;
using System.ComponentModel;

namespace TreeDGraphics.Core.ThreeD
{
    public class Point3D
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public Point3D(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Point3D ToViewPoint(float r, float phi, float teta)
        {
            return new Point3D((float) (-X * Math.Sin(phi) + Y * Math.Cos(phi)),
                (float) (-X * Math.Cos(teta) * Math.Cos(phi) - Y * Math.Cos(teta) * Math.Sin(phi) + Z * Math.Sin(teta)),
                (float) (-X * Math.Sin(teta) * Math.Cos(phi) - Y * Math.Sin(teta) * Math.Sin(phi) - Z * Math.Cos(teta) +
                         r));
        }

        public void Move(Point3D offset)
        {
            X = X + offset.X;
            Y = Y + offset.Y;
            Z = Z + offset.Z;
        }
    }
}