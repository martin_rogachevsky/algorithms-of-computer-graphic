﻿using System.Collections.Generic;
using TreeDGraphics.Core.ThreeD;
using TreeDGraphics.Helpers;
using static System.Math;


namespace TreeDGraphics.Core.Transformers
{
    public static class RotationTransformer
    {
        /// <param name="obj">A figure that should rotate</param>
        /// <param name="axisRotate">Warning! Should be unitary vector</param>
        /// <see>
        ///     <cref>Helpers/VectorHelper.cs:59</cref>
        /// </see>
        /// <param name="angleRotation">Angle of rotation in radian</param>
        /// <returns>New figure that was rotated</returns>
        public static List<Triangle3D> RotateObject(Object3D obj, Point3D axisRotate, double angleRotation)
        {
            var result = new List<Triangle3D>();
            var matrixRotation = GetMatrixRotation(axisRotate, angleRotation);
            foreach (var triangle in obj.Polygons)
            {
                result.Add(new Triangle3D(Rotate(triangle.Lines, matrixRotation)));
            }

            return result;
        }

        public static List<Triangle3D> RotateObject(Object3D obj, Point3D axisRotate, double angleRotation,
            Point3D radiusVector)
        {
            var scalarAxisRadius = radiusVector.ScalarMul(axisRotate);
            var diff = axisRotate.ScalarValMul(scalarAxisRadius).VectSub(radiusVector);
            obj.Move(diff);
            var newObject = new Object3D(RotateObject(obj, axisRotate, angleRotation));
            newObject.Move(diff.ScalarValMul(-1));
            return newObject.Polygons;
        }

        /// <summary>
        ///  Used to rotate the axis of rotation around another axis
        /// </summary>
        /// <param name="axis">The axis that will rotate</param>
        /// <param name="axisRotate">The axis around which will rotate</param>
        /// <param name="angleRotation">Angle of rotation</param>
        /// <returns>Rotated line</returns>
        public static Line3D RotateAxis(Line3D axis, Point3D axisRotate, double angleRotation)
        {
            var matrixRotation = GetMatrixRotation(axisRotate, angleRotation);
            return Rotate(axis, matrixRotation);
        }

        private static List<Line3D> Rotate(List<Line3D> lines, float[][] matrix)
        {
            var result = new List<Line3D>();
            foreach (var line in lines)
            {
                result.Add(Rotate(line, matrix));
            }

            return result;
        }

        private static Line3D Rotate(Line3D line3D, float[][] matrix)
        {
            var start = MatrixHelper.Multiply(line3D.Start, matrix);
            var end = MatrixHelper.Multiply(line3D.End, matrix);
            return new Line3D(start, end, line3D.IsTrash);
        }

        private static float[][] GetMatrixRotation(Point3D axisRotate, double angleRotation)
        {
            var matrix = new float[3][];
            for (var i = 0; i < matrix.Length; i++)
            {
                matrix[i] = new float[3];
            }

            var a = angleRotation;
            var x = axisRotate.X;
            var y = axisRotate.Y;
            var z = axisRotate.Z;

            matrix[0][0] = (float) (Cos(a) + (1 - Cos(a)) * Pow(x, 2));
            matrix[0][1] = (float) ((1 - Cos(a)) * x * y - Sin(a) * z);
            matrix[0][2] = (float) ((1 - Cos(a)) * x * z + Sin(a) * y);

            matrix[1][0] = (float) ((1 - Cos(a)) * y * x + Sin(a) * z);
            matrix[1][1] = (float) (Cos(a) + (1 - Cos(a)) * Pow(y, 2));
            matrix[1][2] = (float) ((1 - Cos(a)) * y * z - Sin(a) * x);

            matrix[2][0] = (float) ((1 - Cos(a)) * z * x - Sin(a) * y);
            matrix[2][1] = (float) ((1 - Cos(a)) * z * y + Sin(a) * x);
            matrix[2][2] = (float) (Cos(a) + (1 - Cos(a)) * Pow(z, 2));
            return matrix;
        }
    }
}