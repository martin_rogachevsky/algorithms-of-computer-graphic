﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tao.OpenGl;
using Tao.FreeGlut;
using Tao.Platform.Windows;

namespace Lab6
{
    public partial class Form1 : Form
    {
        private CylinderRotation CylinderRotation { get; set; }

        private SourceLightPosition SourceLightPosition { get; set; }

        private GLInstance GLInstance { get; set; }

        private Material MaterialProperties { get; set; }

        public Form1()
        {
            InitializeComponent();
            MaterialProperties = new Material();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            GLInstance.Draw();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CylinderRotation = new CylinderRotation(true);
            SourceLightPosition = new SourceLightPosition(false);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            this.CylinderRotation.OnKeyPressed(keyData);
            this.SourceLightPosition.OnKeyPressed(keyData);
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            GLInstance = new GLInstance(Form1.ActiveForm.Width, Form1.ActiveForm.Height, CylinderRotation, SourceLightPosition, MaterialProperties);
            this.Controls.Add(GLInstance.GlControl);
            timer1.Start();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            var enableLightCheckbox = (CheckBox) sender;
            GLInstance.ChangeLight(enableLightCheckbox.Checked);
        }

        private void OnCheckedChanged(object sender, EventArgs e)
        {
            this.CylinderRotation.ChangeState();
            this.SourceLightPosition.ChangeState();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var materialSettingPopup = new MaterialSettingsPopup(MaterialProperties);
            materialSettingPopup.Closed += ActiveFormOnClosed;
            materialSettingPopup.Show();
        }

        private void ActiveFormOnClosed(object sender, EventArgs eventArgs)
        {

        }
    }
}
