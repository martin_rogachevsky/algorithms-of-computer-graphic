﻿using System;

namespace Shapes
{
    internal class RotateUtils
    {
        public static double GetCoefficientRotation(int k, int numberFigure)
        {
            var tang = Math.Tan(k * (Math.PI) / (4 * numberFigure));
            return tang / (tang + 1);
        }
    }
}