﻿using System.Collections.Generic;
using TreeDGraphics.Core.ThreeD;
using TreeDGraphics.Core.TwoD;
using TreeDGraphics.Helpers;
using static System.Math;

namespace TreeDGraphics.Core.Transformers
{
    public static class ProjectionTransformer
    {
        /// <param name="triangles">The list of polygons to be projected</param>
        /// <param name="plane">Triangle description projection plane</param>
        /// <returns>Dictionary of volumetric polygons and flat</returns>
        public static Dictionary<Triangle3D, Polygon2D> AximetricProjection(Object3D obj,
            Triangle3D plane)
        {
            var projection = new Dictionary<Triangle3D, Polygon2D>();
            var matrix = GetMatrixTransformation(plane);
            foreach (var triangle in obj.Polygons)
            {
                projection.Add(triangle, new Polygon2D(Convert(matrix, triangle.Lines), true));
            }

            return projection;
        }

        private static List<Line2D> Convert(float[][] matrixTransformation, List<Line3D> lines3D)
        {
            var lines2D = new List<Line2D>();
            foreach (var line3D in lines3D)
            {
                lines2D.Add(Convert(matrixTransformation, line3D));
            }

            return lines2D;
        }

        private static Line2D Convert(float[][] matrixTransformation, Line3D line3D)
        {
            var start3D = line3D.Start;
            var end3D = line3D.End;
            var start2D = MatrixHelper.Multiply(start3D, matrixTransformation);
            var end2D = MatrixHelper.Multiply(end3D, matrixTransformation);
            return new Line2D(start2D.X, start2D.Y, end2D.X, end2D.Y, line3D.IsTrash);
        }

        private static float[][] GetMatrixTransformation(Triangle3D plane)
        {
            var normalVector = VectorHelper.FindNormalVector(plane);
            var projectionOnPlaneXz = new Point3D(normalVector.X, 0, normalVector.Z);
            var axisZ = new Point3D(0, 0, 1);
            var angleB = Acos(VectorHelper.CosBetweenVector(projectionOnPlaneXz, axisZ));
            var angleA = Acos(VectorHelper.CosBetweenVector(normalVector, projectionOnPlaneXz));
            var matrixTransformation = new float[3][];
            for (var i = 0; i < matrixTransformation.Length; i++)
            {
                matrixTransformation[i] = new float[3];
            }

            matrixTransformation[0][0] = (float)Cos(angleB);
            matrixTransformation[0][1] = (float)(Sin(angleA) * Sin(angleB));
            matrixTransformation[0][2] = 0;

            matrixTransformation[1][0] = 0;
            matrixTransformation[1][1] = (float)Cos(angleA);
            matrixTransformation[1][2] = 0;

            matrixTransformation[2][0] = (float)Sin(angleB);
            matrixTransformation[2][1] = (float)(-Sin(angleA) * Sin(angleB));
            matrixTransformation[2][2] = 0;
            return matrixTransformation;
        }
    }
}
