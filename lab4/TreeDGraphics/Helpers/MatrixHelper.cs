using TreeDGraphics.Core.ThreeD;

namespace TreeDGraphics.Helpers
{
    public class MatrixHelper
    {
        public static Point3D Multiply(Point3D vector, float[][] matrix)
        {
            float x = matrix[0][0] * vector.X + matrix[0][1] * vector.Y + matrix[0][2] * vector.Z;
            float y = matrix[1][0] * vector.X + matrix[1][1] * vector.Y + matrix[1][2] * vector.Z;
            float z = matrix[2][0] * vector.X + matrix[2][1] * vector.Y + matrix[2][2] * vector.Z;
            
            return new Point3D(x, y, z);
        }
    }
}