using TreeDGraphics.Core.TwoD;
using TreeDGraphics.Helpers;

namespace TreeDGraphics.Core.ThreeD
{
    public class Line3D
    {
        public Point3D Start { get; set; }
        public Point3D End { get; set; }
        public bool IsTrash { get; set; }

        public Line3D(Point3D start, Point3D end, bool isTrash = false)
        {
            Start = start;
            End = end;
            IsTrash = isTrash;
        }

        public Point3D DirectionVector => new Point3D(End.X - Start.X, End.Y - Start.Y, End.Z - Start.Z);

        public Line3D Invert()
        {
            return new Line3D(End, Start);
        }

        public Line3D CutLine(float tStart, float tEnd)
        {
            var vect1 = Start.VectAdd(DirectionVector.ScalarValMul(tStart));
            var vect2 = Start.VectAdd(DirectionVector.ScalarValMul(tEnd));
            return new Line3D(new Point3D(vect1.X, vect1.Y, vect1.Z), new Point3D(vect2.X, vect2.Y, vect2.Z));
        }
    }
}