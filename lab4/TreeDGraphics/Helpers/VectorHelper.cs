﻿using System;
using System.Drawing;
using TreeDGraphics.Core.ThreeD;

namespace TreeDGraphics.Helpers
{
    public static class VectorHelper
    {
        public static PointF VectAdd(this PointF v1, PointF v2)
        {
            return new PointF(v1.X + v2.X, v1.Y + v2.Y);
        }

        public static Point3D VectAdd(this Point3D v1, Point3D v2)
        {
            return new Point3D(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
        }

        public static float ScalarMul(this PointF v1, PointF v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y;
        }

        public static float ScalarMul(this Point3D v1, Point3D v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
        }

        public static PointF ScalarValMul(this PointF v1, float val)
        {
            return new PointF(v1.X * val, v1.Y * val);
        }

        public static Point3D ScalarValMul(this Point3D v1, float val)
        {
            return new Point3D(v1.X * val, v1.Y * val, v1.Z * val);
        }

        public static PointF VectSub(this PointF v1, PointF v2)
        {
            return new PointF(v1.X - v2.X, v1.Y - v2.Y);
        }

        public static Point3D VectSub(this Point3D v1, Point3D v2)
        {
            return new Point3D(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
        }

        public static Point3D VectMul(this Point3D v1, Point3D v2)
        {
            return new Point3D(v1.Y * v2.Z - v1.Z * v2.Y, v1.Z * v2.X - v1.X * v2.Z, v1.X * v2.Y - v1.Y * v2.X);
        }

        public static float Length(this Point3D v)
        {
            return (float) Math.Sqrt(v.X * v.X + v.Y * v.Y + v.Z * v.Z);
        }

        public static Point3D UnitaryVector(Point3D vector)
        {
            var length = Length(vector);
            return new Point3D(vector.X / length, vector.Y / length, vector.Z / length);
        }

        public static Point3D FindNormalVector(Triangle3D plane)
        {
            var p1 = plane.Lines[0].Start;
            var p2 = plane.Lines[0].End;
            var p3 = plane.Lines[1].End;
            float
                a1 = p2.X - p1.X,
                a2 = p2.Y - p1.Y,
                a3 = p2.Z - p1.Z,
                b1 = p3.X - p1.X,
                b2 = p3.Y - p1.Y,
                b3 = p3.Z - p3.Z;
            return new Point3D(a2 * b3 - a3 * b2, a3 * b1 - a1 * b3, a1 * b2 - a2 * b1);
        }

        public static double CosBetweenVector(Point3D p1, Point3D p2)
        {
            return
                (p1.X * p2.X + p1.Y * p2.Y + p1.Z * p2.Z) /
                (
                    Math.Sqrt
                    (
                        Math.Pow(p1.X, 2) + Math.Pow(p1.Y, 2) + Math.Pow(p1.Z, 2)
                    ) *
                    Math.Sqrt
                    (
                        Math.Pow(p2.X, 2) + Math.Pow(p2.Y, 2) + Math.Pow(p2.Z, 2)
                    )
                );
        }

        public static Point3D CrossingPoint(this Point3D normalVector, Point3D pointVect, Point3D directionVect, float d)
        {
            return pointVect.VectSub(
                directionVect.ScalarValMul((normalVector.ScalarMul(pointVect) + d) /
                                           normalVector.ScalarMul(directionVect)));
        }
    }
}