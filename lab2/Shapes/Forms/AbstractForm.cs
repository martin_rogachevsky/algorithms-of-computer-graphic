﻿using System;
using System.Threading;
using SDL2;
using Shapes.Figures;
using Shapes.GeometryElements;

namespace Shapes.Forms
{
    public abstract class AbstractForm
    {
        protected IntPtr Renderer;
        private const int ScreenWidth = 640;
        private const int ScreenHeight = 480;

        protected AbstractForm()
        {
            var thread = new Thread(() =>
            {
                SDL.SDL_Init(SDL.SDL_INIT_EVERYTHING);
                var wnd = SDL.SDL_CreateWindow("AOKG Lab 2", 100, 100, 100 + ScreenWidth, 100 + ScreenHeight,
                    SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN);


                Renderer = SDL.SDL_CreateRenderer(wnd, -1, SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED);

                var quit = false;
                while (!quit)
                {
                    SDL.SDL_PollEvent(out var sdlEvent);
                    switch (sdlEvent.type)
                    {
                        case SDL.SDL_EventType.SDL_QUIT:
                        {
                            quit = true;
                            break;
                        }
                        case SDL.SDL_EventType.SDL_KEYDOWN:
                        {
                            var key = sdlEvent.key;
                            switch (key.keysym.sym)
                            {
                                case SDL.SDL_Keycode.SDLK_DOWN:
                                    break;
                                case SDL.SDL_Keycode.SDLK_UP:
                                    break;
                            }

                            break;
                        }
                        case SDL.SDL_EventType.SDL_MOUSEBUTTONDOWN:
                        {
                            if (sdlEvent.button.button == SDL.SDL_BUTTON_LEFT)
                            {
                            }
                            else if (sdlEvent.button.button == SDL.SDL_BUTTON_RIGHT)
                            {
                            }

                            break;
                        }
                    }

                    DrawingAndRotate();
                    Thread.Sleep(10);
                }

                SDL.SDL_DestroyRenderer(Renderer);
                SDL.SDL_DestroyWindow(wnd);
                SDL.SDL_Quit();
            });
            thread.Start();
            thread.Join();
        }

        protected abstract RotatableShape CreateShape();

        private void DrawingAndRotate()
        {
            SDL.SDL_SetRenderDrawColor(Renderer, 255, 255, 255, 255);
            SDL.SDL_RenderClear(Renderer);

            SDL.SDL_SetRenderDrawColor(Renderer, 0, 0, 0, 0);

            RotatableShape triangle = CreateShape();
            for (int countShapes = 0; countShapes < 10; countShapes++)
            {
                DrawShape(triangle);
                triangle.Rotate();
            }

            SDL.SDL_RenderPresent(Renderer);
        }


        private void DrawShape(RotatableShape rotatableShape)
        {
            for (var i = 0; i < rotatableShape.Points.Count; i++)
            {
                Point point1 = rotatableShape.Points[i];
                Point point2;
                // traversing shape
                if (i != rotatableShape.Points.Count - 1)
                {
                    point2 = rotatableShape.Points[i + 1];
                }
                else
                {
                    point2 = rotatableShape.Points[0];
                }
                
                SDL.SDL_RenderDrawLine(Renderer, (int) point1.X, (int) point1.Y, (int) point2.X, (int) point2.Y);
            }
        }
    }
}