using System;
using System.Collections.Generic;
using System.Drawing;
using SDL2;
using TreeDGraphics.Helpers;

namespace TreeDGraphics.Core.TwoD
{
    public class Line2D
    {
        public PointF Start { get; set; }
        public PointF End { get; set; }
        public bool IsTrash { get; set; }
        public readonly List<(float,float)> HiddenParts = new List<(float, float)>(); 

        public Line2D(float sx, float sy, float ex, float ey, bool isTrash = false)
        {
            IsTrash = isTrash;
            Start = new PointF(sx, sy);
            End = new PointF(ex, ey);
        }

        public void Draw(IntPtr renderer, bool isLineShown = true)
        {
            if (IsTrash)
            {
                HiddenParts.Clear();
                HiddenParts.Add((0,1));
            }
            if (HiddenParts.Count > 0)
            {
                foreach (var hiddenPart in HiddenParts)
                {

                    var hiddenLine = CutLine(hiddenPart.Item1, hiddenPart.Item2);
                    var points = hiddenLine.GetDottedLines();
                    foreach (var point in points)
                    {
                        if (isLineShown)
                            SDL.SDL_RenderDrawPoint(renderer, (int) point.X, (int) point.Y);
                    }
                }
                //may be some bugs. need rotation for testing
                float t = 0;
                HiddenParts?.Sort((x, y) => x.Item1.CompareTo(y.Item1));
                var hiddenEnum = HiddenParts.GetEnumerator();
                hiddenEnum.MoveNext();
                while (t < 1)
                {
                    var n = hiddenEnum.Current;
                    if (t < n.Item1)
                        CutLine(t, n.Item1).Draw(renderer);
                    t = n.Item2;
                    if (!hiddenEnum.MoveNext())
                    {
                        CutLine(t, 1).Draw(renderer);
                        break;
                    }
                }

            }
            else
            {
                SDL.SDL_RenderDrawLine(renderer, (int) Start.X, (int) Start.Y, (int) End.X, (int) End.Y);
            }

        }

        public PointF NormalVect() =>
            new PointF(Start.Y - End.Y, End.X - Start.X);

        public PointF DirectVect() =>
            new PointF(End.X - Start.X, End.Y - Start.Y);

        public Line2D Invert() =>
            new Line2D(End.X, End.Y, Start.X, Start.Y);

        public Line2D CutLine(float tStart, float tEnd)
        {
            var vect1 = Start.VectAdd(DirectVect().ScalarValMul(tStart));
            var vect2 = Start.VectAdd(DirectVect().ScalarValMul(tEnd));
            return new Line2D(vect1.X, vect1.Y, vect2.X, vect2.Y);
        }

        public bool IsLeft(Line2D observingLine)
        {
            var d = DirectVect();
            var s = observingLine.Start.VectSub(Start);
            return d.X * s.Y - d.Y * s.X >= 0;
        }
    }
}