﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab6
{
    public partial class MaterialSettingsPopup : Form
    {
        private Material material { get; set; }

        public MaterialSettingsPopup(Material material)
        {
            InitializeComponent();
            this.material = material;
            SetupBindings();
        }

        private void SetupBindings()
        {
            BindFront();
            BindBack();
        }

        private void BindFront()
        {
            frontAmbientXInput.DataBindings.Add("Text", material.FrontMaterialProperties.Ambient, "X", true, DataSourceUpdateMode.OnPropertyChanged);
            frontAmbientYInput.DataBindings.Add("Text", material.FrontMaterialProperties.Ambient, "Y", true, DataSourceUpdateMode.OnPropertyChanged);
            frontAmbientZInput.DataBindings.Add("Text", material.FrontMaterialProperties.Ambient, "Z", true, DataSourceUpdateMode.OnPropertyChanged);

            frontDiffuseXInput.DataBindings.Add("Text", material.FrontMaterialProperties.Diffuse, "X", true, DataSourceUpdateMode.OnPropertyChanged);
            frontDiffuseYInput.DataBindings.Add("Text", material.FrontMaterialProperties.Diffuse, "Y", true, DataSourceUpdateMode.OnPropertyChanged);
            frontDiffuseZInput.DataBindings.Add("Text", material.FrontMaterialProperties.Diffuse, "Z", true, DataSourceUpdateMode.OnPropertyChanged);

            frontEmissionXInput.DataBindings.Add("Text", material.FrontMaterialProperties.Emission, "X", true, DataSourceUpdateMode.OnPropertyChanged);
            frontEmissionYInput.DataBindings.Add("Text", material.FrontMaterialProperties.Emission, "Y", true, DataSourceUpdateMode.OnPropertyChanged);
            frontEmissionZInput.DataBindings.Add("Text", material.FrontMaterialProperties.Emission, "Z", true, DataSourceUpdateMode.OnPropertyChanged);

            frontSpecularXInput.DataBindings.Add("Text", material.FrontMaterialProperties.Specular, "X", true, DataSourceUpdateMode.OnPropertyChanged);
            frontSpecularYInput.DataBindings.Add("Text", material.FrontMaterialProperties.Specular, "Y", true, DataSourceUpdateMode.OnPropertyChanged);
            frontSpecularZInput.DataBindings.Add("Text", material.FrontMaterialProperties.Specular, "Z", true, DataSourceUpdateMode.OnPropertyChanged);

            frontShininessInput.DataBindings.Add("Text", material.FrontMaterialProperties.Shininess, "W", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void BindBack()
        {
            backAmbientXInput.DataBindings.Add("Text", material.BackMaterialProperties.Ambient, "X", true, DataSourceUpdateMode.OnPropertyChanged);
            backAmbientYInput.DataBindings.Add("Text", material.BackMaterialProperties.Ambient, "Y", true, DataSourceUpdateMode.OnPropertyChanged);
            backAmbientZInput.DataBindings.Add("Text", material.BackMaterialProperties.Ambient, "Z", true, DataSourceUpdateMode.OnPropertyChanged);

            backDiffuseXInput.DataBindings.Add("Text", material.BackMaterialProperties.Diffuse, "X", true, DataSourceUpdateMode.OnPropertyChanged);
            backDiffuseYInput.DataBindings.Add("Text", material.BackMaterialProperties.Diffuse, "Y", true, DataSourceUpdateMode.OnPropertyChanged);
            backDiffuseZInput.DataBindings.Add("Text", material.BackMaterialProperties.Diffuse, "Z", true, DataSourceUpdateMode.OnPropertyChanged);

            backEmissionXInput.DataBindings.Add("Text", material.BackMaterialProperties.Emission, "X", true, DataSourceUpdateMode.OnPropertyChanged);
            backEmissionYInput.DataBindings.Add("Text", material.BackMaterialProperties.Emission, "Y", true, DataSourceUpdateMode.OnPropertyChanged);
            backEmissionZInput.DataBindings.Add("Text", material.BackMaterialProperties.Emission, "Z", true, DataSourceUpdateMode.OnPropertyChanged);

            backSpecularXInput.DataBindings.Add("Text", material.BackMaterialProperties.Specular, "X", true, DataSourceUpdateMode.OnPropertyChanged);
            backSpecularYInput.DataBindings.Add("Text", material.BackMaterialProperties.Specular, "Y", true, DataSourceUpdateMode.OnPropertyChanged);
            backSpecularZInput.DataBindings.Add("Text", material.BackMaterialProperties.Specular, "Z", true, DataSourceUpdateMode.OnPropertyChanged);

            backShininessInput.DataBindings.Add("Text", material.BackMaterialProperties.Specular, "W", true, DataSourceUpdateMode.OnPropertyChanged);
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            MaterialSettingsPopup.ActiveForm.Close();
        }
    }
}
