﻿using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using SDL2;

namespace Lab1
{
    public partial class MainForm : Form
    {
        private IntPtr renderer;
        const int SCREEN_WIDTH = 640;
        const int SCREEN_HEIGHT = 480;
       // private SDL.SDL_Point []points = new SDL.SDL_Point[SCREEN_WIDTH * SCREEN_HEIGHT];
        public MainForm()
        {
            InitializeComponent();

            Thread thread = new Thread(() =>
            {
                SDL.SDL_Init(SDL.SDL_INIT_EVERYTHING);
                IntPtr wnd = SDL.SDL_CreateWindow("AOKG Lab 1", 100 , 100, 100 + SCREEN_WIDTH, 100 + SCREEN_HEIGHT, 
                    /*SDL.SDL_WindowFlags.SDL_WINDOW_RESIZABLE |*/
                                                                                  SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN);
              //  var shape = new Shape();

                renderer = SDL.SDL_CreateRenderer(wnd, -1, SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED);

                var archimedesSpiral = new ArchimedesSpiral(SCREEN_WIDTH, SCREEN_HEIGHT, 0.05, 0.01, 0.005);

                bool quit = false;
                while (!quit)
                {
                    SDL.SDL_Event sdlEvent;
                    SDL.SDL_PollEvent(out sdlEvent);
                    switch (sdlEvent.type)
                    {
                        case SDL.SDL_EventType.SDL_QUIT:
                        {
                            quit = true;
                            break;
                        }
                        case SDL.SDL_EventType.SDL_KEYDOWN:
                        {
                            var key = sdlEvent.key;
                            switch (key.keysym.sym)
                            {
                                case SDL.SDL_Keycode.SDLK_UP:
                                case SDL.SDL_Keycode.SDLK_DOWN:
                                {
                                    archimedesSpiral.OnMove(key.keysym.sym);
                                    break;
                                }
                                case SDL.SDL_Keycode.SDLK_LEFT:
                                case SDL.SDL_Keycode.SDLK_RIGHT:
                                {
                                    archimedesSpiral.OnRotate(key.keysym.sym);
                                    break;
                                }
                            }
                            break;
                        }
                        case SDL.SDL_EventType.SDL_MOUSEBUTTONDOWN:
                        {
                            if (sdlEvent.button.button == SDL.SDL_BUTTON_LEFT)
                            {
                                // do smth
                            } else
                            if (sdlEvent.button.button == SDL.SDL_BUTTON_RIGHT)
                            {
                                // do smth
                            }
                            break;
                        }
                    
                    }
                    DrawShape(archimedesSpiral);
                    Thread.Sleep(10); // somehow calibrate render loop
                }
                SDL.SDL_DestroyRenderer(renderer);
                SDL.SDL_DestroyWindow(wnd);
                SDL.SDL_Quit();

            });
            thread.Start();
            thread.Join();
        }

        private void DrawShape(ArchimedesSpiral archimedesSpiral)
        {
            SDL.SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
            SDL.SDL_RenderClear(renderer);
            // Формат цвета в HEX коде:
            //     0x00RRGGBB
            //  где R: от 00 до FF
            //      G: от 00 до FF
            //      B: от 00 до FF       
            SDL.SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
           
            SDL.SDL_RenderDrawPoints(renderer, archimedesSpiral.points.ToArray(), archimedesSpiral.points.Count);
            SDL.SDL_RenderPresent(renderer);
        }   

        private void MainForm_Shown(object sender, EventArgs e)
        {
            Hide();
            Close();
        }
    }
}
