﻿using Tao.FreeGlut;
using Tao.OpenGl;
using Tao.Platform.Windows;

namespace Lab6
{
    public class GLInstance
    {
        public SimpleOpenGlControl GlControl { get; set; }

        private CylinderRotation cylinderRotation { get; set; }
        private SourceLightPosition sourceLightPosition { get; set; }
        private Material material { get; set; }

        public GLInstance(int width, int height, CylinderRotation cylinderRotation,
            SourceLightPosition sourceLightPosition, Material material)
        {
            this.cylinderRotation = cylinderRotation;
            this.sourceLightPosition = sourceLightPosition;
            this.material = material;
            GlControl = new SimpleOpenGlControl();
            GlControl.InitializeContexts();
            GlControl.Width = width;
            GlControl.Height = height;
            InitializeGlut();
        }

        public void Draw()
        {
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glClearColor(255, 255, 255, 1);
            Gl.glLoadIdentity();
            Gl.glPushMatrix();
            Gl.glTranslated(0, 0, -5);
            Gl.glRotated(cylinderRotation.Angle, cylinderRotation.RotationX, cylinderRotation.RotationY, cylinderRotation.RotationZ);
            Gl.glScaled(1, 1, 1);

            DrawShape();

            float[] light0_position = { sourceLightPosition.PositionX, sourceLightPosition.PositionY, sourceLightPosition.PositionZ, 1.0f };
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_POSITION, light0_position);
            ApplyMaterialProperties();
            Gl.glPopMatrix();
            Gl.glFlush();
            GlControl.Invalidate();
        }

        public void ChangeLight(bool isChecked)
        {
            if (isChecked)
            {
                Gl.glEnable(Gl.GL_LIGHT0);
            }
            else
            {
                Gl.glDisable(Gl.GL_LIGHT0);
            }
        }

        private void DrawShape()
        {
            Gl.glBegin(Gl.GL_QUADS);
                Gl.glVertex3f(0.0f, 1.0f, 1.0f);
                Gl.glVertex3f(0.0f, 0.25f, 1.0f);
                Gl.glVertex3f(-0.25f, -0.25f, 1.0f);
                Gl.glVertex3f(-1.0f, -1.0f, 1.0f);

                Gl.glVertex3f(0.0f, 1.0f, 1.0f);
                Gl.glVertex3f(0.0f, 0.25f, 1.0f);
                Gl.glVertex3f(0.25f, -0.25f, 1.0f);
                Gl.glVertex3f(1.0f, -1.0f, 1.0f);

                Gl.glVertex3f(-1.0f, -1.0f, 1.0f);
                Gl.glVertex3f(-0.25f, -0.25f, 1.0f);
                Gl.glVertex3f(0.25f, -0.25f, 1.0f);
                Gl.glVertex3f(1.0f, -1.0f, 1.0f);

                Gl.glVertex3f(0.0f, 1.0f, -1.0f);
                Gl.glVertex3f(0.0f, 0.25f, -1.0f);
                Gl.glVertex3f(-0.25f, -0.25f, -1.0f);
                Gl.glVertex3f(-1.0f, -1.0f, -1.0f);

                Gl.glVertex3f(0.0f, 1.0f, -1.0f);
                Gl.glVertex3f(0.0f, 0.25f, -1.0f);
                Gl.glVertex3f(0.25f, -0.25f, -1.0f);
                Gl.glVertex3f(1.0f, -1.0f, -1.0f);

                Gl.glVertex3f(-1.0f, -1.0f, -1.0f);
                Gl.glVertex3f(-0.25f, -0.25f, -1.0f);
                Gl.glVertex3f(0.25f, -0.25f, -1.0f);
                Gl.glVertex3f(1.0f, -1.0f, -1.0f);

                Gl.glVertex3f(-1.0f, -1.0f, 1.0f);
                Gl.glVertex3f(1.0f, -1.0f, 1.0f);
                Gl.glVertex3f(1.0f, -1.0f, -1.0f);
                Gl.glVertex3f(-1.0f, -1.0f, -1.0f);

                Gl.glVertex3f(1.0f, -1.0f, 1.0f);
                Gl.glVertex3f(0.0f, 1.0f, 1.0f);
                Gl.glVertex3f(0.0f, 1.0f, -1.0f);
                Gl.glVertex3f(1.0f, -1.0f, -1.0f);

                Gl.glVertex3f(-1.0f, -1.0f, 1.0f);
                Gl.glVertex3f(0.0f, 1.0f, 1.0f);
                Gl.glVertex3f(0.0f, 1.0f, -1.0f);
                Gl.glVertex3f(-1.0f, -1.0f, -1.0f);
            Gl.glEnd();
        }

        private void InitializeGlut()
        {
            Glut.glutInit();
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE);
            Gl.glClearColor(255, 255, 255, 1);
            Gl.glViewport(0, 0, GlControl.Width, GlControl.Height);
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Glu.gluPerspective(45, GlControl.Width / (float)GlControl.Height, 0.1, 200);
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            float[] light0_position = { sourceLightPosition.PositionX, sourceLightPosition.PositionY, sourceLightPosition.PositionZ, 1.0f };
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_POSITION, light0_position);
            ApplyMaterialProperties();
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glEnable(Gl.GL_LIGHT0);
        }

        private void ApplyMaterialProperties()
        {
            ApplyFrontMaterialProperties();
            ApplyBackMaterialProperties();
        }

        private void ApplyFrontMaterialProperties()
        {
            Gl.glMaterialfv(Gl.GL_FRONT, Gl.GL_AMBIENT, new float[]
            {
                material.FrontMaterialProperties.Ambient.X.Value,
                material.FrontMaterialProperties.Ambient.Y.Value,
                material.FrontMaterialProperties.Ambient.Z.Value,
                material.FrontMaterialProperties.Ambient.W.Value
            });

            Gl.glMaterialfv(Gl.GL_FRONT, Gl.GL_DIFFUSE, new float[]
            {
                material.FrontMaterialProperties.Diffuse.X.Value,
                material.FrontMaterialProperties.Diffuse.Y.Value,
                material.FrontMaterialProperties.Diffuse.Z.Value,
                material.FrontMaterialProperties.Diffuse.W.Value,
            });

            Gl.glMaterialfv(Gl.GL_FRONT, Gl.GL_SPECULAR, new float[]
            {
                material.FrontMaterialProperties.Specular.X.Value,
                material.FrontMaterialProperties.Specular.Y.Value,
                material.FrontMaterialProperties.Specular.Z.Value,
                material.FrontMaterialProperties.Specular.W.Value,
            });

            Gl.glMaterialfv(Gl.GL_FRONT, Gl.GL_EMISSION, new float[]
            {
                material.FrontMaterialProperties.Emission.X.Value,
                material.FrontMaterialProperties.Emission.Y.Value,
                material.FrontMaterialProperties.Emission.Z.Value,
                material.FrontMaterialProperties.Emission.W.Value,
            });

            Gl.glMaterialf(Gl.GL_FRONT, Gl.GL_SHININESS, material.FrontMaterialProperties.Shininess.W.Value);
        }

        private void ApplyBackMaterialProperties()
        {
            Gl.glMaterialfv(Gl.GL_BACK, Gl.GL_AMBIENT, new float[]
            {
                material.BackMaterialProperties.Ambient.X.Value,
                material.BackMaterialProperties.Ambient.Y.Value,
                material.BackMaterialProperties.Ambient.Z.Value,
                material.BackMaterialProperties.Ambient.W.Value
            });

            Gl.glMaterialfv(Gl.GL_BACK, Gl.GL_DIFFUSE, new float[]
            {
                material.BackMaterialProperties.Diffuse.X.Value,
                material.BackMaterialProperties.Diffuse.Y.Value,
                material.BackMaterialProperties.Diffuse.Z.Value,
                material.BackMaterialProperties.Diffuse.W.Value,
            });

            Gl.glMaterialfv(Gl.GL_BACK, Gl.GL_SPECULAR, new float[]
            {
                material.BackMaterialProperties.Specular.X.Value,
                material.BackMaterialProperties.Specular.Y.Value,
                material.BackMaterialProperties.Specular.Z.Value,
                material.BackMaterialProperties.Specular.W.Value,
            });

            Gl.glMaterialfv(Gl.GL_BACK, Gl.GL_EMISSION, new float[]
            {
                material.BackMaterialProperties.Emission.X.Value,
                material.BackMaterialProperties.Emission.Y.Value,
                material.BackMaterialProperties.Emission.Z.Value,
                material.BackMaterialProperties.Emission.W.Value,
            });

            Gl.glMaterialf(Gl.GL_BACK, Gl.GL_SHININESS, material.BackMaterialProperties.Shininess.W.Value);
        }
    }
}
