﻿using System;
using System.Collections.Generic;
using TreeDGraphics.Core.ThreeD;
using TreeDGraphics.Core.TwoD;
using TreeDGraphics.Helpers;

namespace TreeDGraphics.Core.Drawers
{
    public class PerspectiveProjectionDrawer
    {
        private readonly Object3D _projectionSource;
        private readonly IDictionary<Triangle3D, Polygon2D> _projectionBase;
        private readonly float _r, _d, _teta, _phi;
        private readonly Point3D _observingPoint, _displayNormalVector;

        public PerspectiveProjectionDrawer(List<Triangle3D> projectionSource, float r, float d, int teta, int phi)
        {
            _projectionSource = new Object3D(projectionSource);
            _projectionSource.MoveToCoordinateSystemStart();
            _r = r;
            _teta = (float) ((float) teta / 180 * Math.PI);
            _phi = (float) ((float) phi / 180 * Math.PI);
            _projectionBase = new Dictionary<Triangle3D, Polygon2D>();
            _observingPoint = new Point3D((float) (_r * Math.Sin(_teta) * Math.Cos(_phi)),
                (float) (_r * Math.Sin(_teta) * Math.Sin(_phi)), (float) (_r * Math.Cos(_teta)));
            _displayNormalVector =
                new Line3D(_observingPoint, new Point3D(0, 0, 0)).DirectionVector.ScalarValMul((float) (1.0 / (r - d)));
            BuildProjectionBase();
        }

        private void BuildProjectionBase()
        {
            foreach (var triangle3D in _projectionSource.Polygons)
            {
                var result = new Polygon2D(true);
                foreach (var line in triangle3D.Lines)
                {
                    var start = _displayNormalVector.CrossingPoint(line.Start,
                        new Line3D(line.Start, _observingPoint).DirectionVector, _r - _d);
                    var vs = start.ToViewPoint(_r, _phi, _teta);
                    var end = _displayNormalVector.CrossingPoint(line.End,
                        new Line3D(line.End, _observingPoint).DirectionVector, _r - _d);
                    var ve = end.ToViewPoint(_r, _phi, _teta);
                    result.Lines.Add(new Line2D(vs.X + 250, vs.Y + 250, ve.X + 250, ve.Y + 250, line.IsTrash));
                }

                _projectionBase.Add(triangle3D, result);
            }
        }

        private bool IsLineBehindSurface((float, float) crossingParams, Line3D originalLine,
            Triangle3D originalTriangle)
        {
            Point3D analyticalPoint;
            if (Math.Abs(crossingParams.Item1) < 0.01 && Math.Abs(crossingParams.Item2 - 1) < 0.01)
            {
                analyticalPoint = originalLine.CutLine(0, (float) 0.5).End;
            }
            else
            {
                if (crossingParams.Item1 > 0 && Math.Abs(crossingParams.Item2 - 1) < 0.01)
                {
                    analyticalPoint = originalLine.CutLine(0, crossingParams.Item1).End;
                }
                else
                {
                    analyticalPoint = originalLine.CutLine(crossingParams.Item1, crossingParams.Item2).End;
                }
            }

            var distToLine = _observingPoint.VectSub(analyticalPoint).Length();
            var crossingPoint = originalTriangle.CrossingPoint(analyticalPoint, _observingPoint);
            return Triangle3D.DistanceTo(_observingPoint, crossingPoint) <= distToLine;
        }

        private void BuildProjection()
        {
            foreach (var master in _projectionBase)
            {
                foreach (var slave in _projectionBase)
                {
                    for (int i = 0; i < slave.Value.Lines.Count; i++)
                    {
                        if (slave.Value.Lines[i].IsTrash)
                            continue;

                        var crossingPoints = CyrusBeck.CrossByLine(master.Value, slave.Value.Lines[i]);
                        if (crossingPoints.Item1 < 0.05 && crossingPoints.Item2 < 0.05)
                            continue;
                        var lineBehind = IsLineBehindSurface(crossingPoints, slave.Key.Lines[i], master.Key);
                        if (lineBehind)
                        {
                            slave.Value.Lines[i].HiddenParts.Add(crossingPoints);
                        }                       
                    }
                }
            }
        }

        public void Draw(IntPtr renderer)
        {
            BuildProjection();
            foreach (var polygon2D in _projectionBase.Values)
            {
                foreach (var line in polygon2D.Lines)
                {
                    if (!line.IsTrash)
                        line.Draw(renderer);
                }
            }
        }
    }
}