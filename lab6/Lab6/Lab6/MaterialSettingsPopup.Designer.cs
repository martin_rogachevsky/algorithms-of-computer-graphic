﻿namespace Lab6
{
    partial class MaterialSettingsPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.frontGroupBox = new System.Windows.Forms.GroupBox();
            this.frontShininessInput = new System.Windows.Forms.MaskedTextBox();
            this.frontEmissionZInput = new System.Windows.Forms.MaskedTextBox();
            this.frontEmissionYInput = new System.Windows.Forms.MaskedTextBox();
            this.frontEmissionXInput = new System.Windows.Forms.MaskedTextBox();
            this.frontSpecularZInput = new System.Windows.Forms.MaskedTextBox();
            this.frontSpecularYInput = new System.Windows.Forms.MaskedTextBox();
            this.frontSpecularXInput = new System.Windows.Forms.MaskedTextBox();
            this.frontDiffuseZInput = new System.Windows.Forms.MaskedTextBox();
            this.frontDiffuseYInput = new System.Windows.Forms.MaskedTextBox();
            this.frontDiffuseXInput = new System.Windows.Forms.MaskedTextBox();
            this.frontAmbientZInput = new System.Windows.Forms.MaskedTextBox();
            this.frontAmbientYInput = new System.Windows.Forms.MaskedTextBox();
            this.frontAmbientXInput = new System.Windows.Forms.MaskedTextBox();
            this.FrontShininessLabel = new System.Windows.Forms.Label();
            this.FrontEmissionLabel = new System.Windows.Forms.Label();
            this.FrontSpecularLabel = new System.Windows.Forms.Label();
            this.FrontDiffuseLabel = new System.Windows.Forms.Label();
            this.FrontAmbientLabel = new System.Windows.Forms.Label();
            this.backGroupBox = new System.Windows.Forms.GroupBox();
            this.backShininessInput = new System.Windows.Forms.MaskedTextBox();
            this.backEmissionZInput = new System.Windows.Forms.MaskedTextBox();
            this.backEmissionYInput = new System.Windows.Forms.MaskedTextBox();
            this.backEmissionXInput = new System.Windows.Forms.MaskedTextBox();
            this.backSpecularZInput = new System.Windows.Forms.MaskedTextBox();
            this.backSpecularYInput = new System.Windows.Forms.MaskedTextBox();
            this.backSpecularXInput = new System.Windows.Forms.MaskedTextBox();
            this.backDiffuseZInput = new System.Windows.Forms.MaskedTextBox();
            this.backDiffuseYInput = new System.Windows.Forms.MaskedTextBox();
            this.backDiffuseXInput = new System.Windows.Forms.MaskedTextBox();
            this.backAmbientZInput = new System.Windows.Forms.MaskedTextBox();
            this.backAmbientYInput = new System.Windows.Forms.MaskedTextBox();
            this.backAmbientXInput = new System.Windows.Forms.MaskedTextBox();
            this.BackShininessLabel = new System.Windows.Forms.Label();
            this.BackEmissionLabel = new System.Windows.Forms.Label();
            this.BackSpecularLabel = new System.Windows.Forms.Label();
            this.BackDiffuseLabel = new System.Windows.Forms.Label();
            this.BackAmbientLabel = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.frontGroupBox.SuspendLayout();
            this.backGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // frontGroupBox
            // 
            this.frontGroupBox.Controls.Add(this.frontShininessInput);
            this.frontGroupBox.Controls.Add(this.frontEmissionZInput);
            this.frontGroupBox.Controls.Add(this.frontEmissionYInput);
            this.frontGroupBox.Controls.Add(this.frontEmissionXInput);
            this.frontGroupBox.Controls.Add(this.frontSpecularZInput);
            this.frontGroupBox.Controls.Add(this.frontSpecularYInput);
            this.frontGroupBox.Controls.Add(this.frontSpecularXInput);
            this.frontGroupBox.Controls.Add(this.frontDiffuseZInput);
            this.frontGroupBox.Controls.Add(this.frontDiffuseYInput);
            this.frontGroupBox.Controls.Add(this.frontDiffuseXInput);
            this.frontGroupBox.Controls.Add(this.frontAmbientZInput);
            this.frontGroupBox.Controls.Add(this.frontAmbientYInput);
            this.frontGroupBox.Controls.Add(this.frontAmbientXInput);
            this.frontGroupBox.Controls.Add(this.FrontShininessLabel);
            this.frontGroupBox.Controls.Add(this.FrontEmissionLabel);
            this.frontGroupBox.Controls.Add(this.FrontSpecularLabel);
            this.frontGroupBox.Controls.Add(this.FrontDiffuseLabel);
            this.frontGroupBox.Controls.Add(this.FrontAmbientLabel);
            this.frontGroupBox.Location = new System.Drawing.Point(12, 12);
            this.frontGroupBox.Name = "frontGroupBox";
            this.frontGroupBox.Size = new System.Drawing.Size(241, 199);
            this.frontGroupBox.TabIndex = 0;
            this.frontGroupBox.TabStop = false;
            this.frontGroupBox.Text = "Front";
            // 
            // frontShininessInput
            // 
            this.frontShininessInput.Location = new System.Drawing.Point(96, 139);
            this.frontShininessInput.Mask = "000";
            this.frontShininessInput.Name = "frontShininessInput";
            this.frontShininessInput.Size = new System.Drawing.Size(36, 22);
            this.frontShininessInput.TabIndex = 17;
            // 
            // frontEmissionZInput
            // 
            this.frontEmissionZInput.Location = new System.Drawing.Point(180, 112);
            this.frontEmissionZInput.Mask = "0.0";
            this.frontEmissionZInput.Name = "frontEmissionZInput";
            this.frontEmissionZInput.Size = new System.Drawing.Size(36, 22);
            this.frontEmissionZInput.TabIndex = 16;
            // 
            // frontEmissionYInput
            // 
            this.frontEmissionYInput.Location = new System.Drawing.Point(138, 112);
            this.frontEmissionYInput.Mask = "0.0";
            this.frontEmissionYInput.Name = "frontEmissionYInput";
            this.frontEmissionYInput.Size = new System.Drawing.Size(36, 22);
            this.frontEmissionYInput.TabIndex = 15;
            // 
            // frontEmissionXInput
            // 
            this.frontEmissionXInput.Location = new System.Drawing.Point(96, 112);
            this.frontEmissionXInput.Mask = "0.0";
            this.frontEmissionXInput.Name = "frontEmissionXInput";
            this.frontEmissionXInput.Size = new System.Drawing.Size(36, 22);
            this.frontEmissionXInput.TabIndex = 14;
            // 
            // frontSpecularZInput
            // 
            this.frontSpecularZInput.Location = new System.Drawing.Point(180, 85);
            this.frontSpecularZInput.Mask = "0.0";
            this.frontSpecularZInput.Name = "frontSpecularZInput";
            this.frontSpecularZInput.Size = new System.Drawing.Size(36, 22);
            this.frontSpecularZInput.TabIndex = 13;
            // 
            // frontSpecularYInput
            // 
            this.frontSpecularYInput.Location = new System.Drawing.Point(138, 85);
            this.frontSpecularYInput.Mask = "0.0";
            this.frontSpecularYInput.Name = "frontSpecularYInput";
            this.frontSpecularYInput.Size = new System.Drawing.Size(36, 22);
            this.frontSpecularYInput.TabIndex = 12;
            // 
            // frontSpecularXInput
            // 
            this.frontSpecularXInput.Location = new System.Drawing.Point(96, 85);
            this.frontSpecularXInput.Mask = "0.0";
            this.frontSpecularXInput.Name = "frontSpecularXInput";
            this.frontSpecularXInput.Size = new System.Drawing.Size(36, 22);
            this.frontSpecularXInput.TabIndex = 11;
            // 
            // frontDiffuseZInput
            // 
            this.frontDiffuseZInput.Location = new System.Drawing.Point(180, 58);
            this.frontDiffuseZInput.Mask = "0.0";
            this.frontDiffuseZInput.Name = "frontDiffuseZInput";
            this.frontDiffuseZInput.Size = new System.Drawing.Size(36, 22);
            this.frontDiffuseZInput.TabIndex = 10;
            // 
            // frontDiffuseYInput
            // 
            this.frontDiffuseYInput.Location = new System.Drawing.Point(138, 58);
            this.frontDiffuseYInput.Mask = "0.0";
            this.frontDiffuseYInput.Name = "frontDiffuseYInput";
            this.frontDiffuseYInput.Size = new System.Drawing.Size(36, 22);
            this.frontDiffuseYInput.TabIndex = 9;
            // 
            // frontDiffuseXInput
            // 
            this.frontDiffuseXInput.Location = new System.Drawing.Point(96, 58);
            this.frontDiffuseXInput.Mask = "0.0";
            this.frontDiffuseXInput.Name = "frontDiffuseXInput";
            this.frontDiffuseXInput.Size = new System.Drawing.Size(36, 22);
            this.frontDiffuseXInput.TabIndex = 8;
            // 
            // frontAmbientZInput
            // 
            this.frontAmbientZInput.Location = new System.Drawing.Point(180, 31);
            this.frontAmbientZInput.Mask = "0.0";
            this.frontAmbientZInput.Name = "frontAmbientZInput";
            this.frontAmbientZInput.Size = new System.Drawing.Size(36, 22);
            this.frontAmbientZInput.TabIndex = 7;
            // 
            // frontAmbientYInput
            // 
            this.frontAmbientYInput.Location = new System.Drawing.Point(138, 31);
            this.frontAmbientYInput.Mask = "0.0";
            this.frontAmbientYInput.Name = "frontAmbientYInput";
            this.frontAmbientYInput.Size = new System.Drawing.Size(36, 22);
            this.frontAmbientYInput.TabIndex = 6;
            // 
            // frontAmbientXInput
            // 
            this.frontAmbientXInput.Location = new System.Drawing.Point(96, 31);
            this.frontAmbientXInput.Mask = "0.0";
            this.frontAmbientXInput.Name = "frontAmbientXInput";
            this.frontAmbientXInput.Size = new System.Drawing.Size(36, 22);
            this.frontAmbientXInput.TabIndex = 5;
            this.frontAmbientXInput.ValidatingType = typeof(System.DateTime);
            // 
            // FrontShininessLabel
            // 
            this.FrontShininessLabel.AutoSize = true;
            this.FrontShininessLabel.Location = new System.Drawing.Point(6, 142);
            this.FrontShininessLabel.Name = "FrontShininessLabel";
            this.FrontShininessLabel.Size = new System.Drawing.Size(69, 17);
            this.FrontShininessLabel.TabIndex = 4;
            this.FrontShininessLabel.Text = "Shininess";
            // 
            // FrontEmissionLabel
            // 
            this.FrontEmissionLabel.AutoSize = true;
            this.FrontEmissionLabel.Location = new System.Drawing.Point(6, 115);
            this.FrontEmissionLabel.Name = "FrontEmissionLabel";
            this.FrontEmissionLabel.Size = new System.Drawing.Size(64, 17);
            this.FrontEmissionLabel.TabIndex = 3;
            this.FrontEmissionLabel.Text = "Emission";
            // 
            // FrontSpecularLabel
            // 
            this.FrontSpecularLabel.AutoSize = true;
            this.FrontSpecularLabel.Location = new System.Drawing.Point(6, 88);
            this.FrontSpecularLabel.Name = "FrontSpecularLabel";
            this.FrontSpecularLabel.Size = new System.Drawing.Size(64, 17);
            this.FrontSpecularLabel.TabIndex = 2;
            this.FrontSpecularLabel.Text = "Specular";
            // 
            // FrontDiffuseLabel
            // 
            this.FrontDiffuseLabel.AutoSize = true;
            this.FrontDiffuseLabel.Location = new System.Drawing.Point(6, 61);
            this.FrontDiffuseLabel.Name = "FrontDiffuseLabel";
            this.FrontDiffuseLabel.Size = new System.Drawing.Size(52, 17);
            this.FrontDiffuseLabel.TabIndex = 1;
            this.FrontDiffuseLabel.Text = "Diffuse";
            // 
            // FrontAmbientLabel
            // 
            this.FrontAmbientLabel.AutoSize = true;
            this.FrontAmbientLabel.Location = new System.Drawing.Point(6, 34);
            this.FrontAmbientLabel.Name = "FrontAmbientLabel";
            this.FrontAmbientLabel.Size = new System.Drawing.Size(59, 17);
            this.FrontAmbientLabel.TabIndex = 0;
            this.FrontAmbientLabel.Text = "Ambient";
            // 
            // backGroupBox
            // 
            this.backGroupBox.Controls.Add(this.backShininessInput);
            this.backGroupBox.Controls.Add(this.backEmissionZInput);
            this.backGroupBox.Controls.Add(this.backEmissionYInput);
            this.backGroupBox.Controls.Add(this.backEmissionXInput);
            this.backGroupBox.Controls.Add(this.backSpecularZInput);
            this.backGroupBox.Controls.Add(this.backSpecularYInput);
            this.backGroupBox.Controls.Add(this.backSpecularXInput);
            this.backGroupBox.Controls.Add(this.backDiffuseZInput);
            this.backGroupBox.Controls.Add(this.backDiffuseYInput);
            this.backGroupBox.Controls.Add(this.backDiffuseXInput);
            this.backGroupBox.Controls.Add(this.backAmbientZInput);
            this.backGroupBox.Controls.Add(this.backAmbientYInput);
            this.backGroupBox.Controls.Add(this.backAmbientXInput);
            this.backGroupBox.Controls.Add(this.BackShininessLabel);
            this.backGroupBox.Controls.Add(this.BackEmissionLabel);
            this.backGroupBox.Controls.Add(this.BackSpecularLabel);
            this.backGroupBox.Controls.Add(this.BackDiffuseLabel);
            this.backGroupBox.Controls.Add(this.BackAmbientLabel);
            this.backGroupBox.Location = new System.Drawing.Point(259, 12);
            this.backGroupBox.Name = "backGroupBox";
            this.backGroupBox.Size = new System.Drawing.Size(241, 199);
            this.backGroupBox.TabIndex = 20;
            this.backGroupBox.TabStop = false;
            this.backGroupBox.Text = "Back";
            // 
            // backShininessInput
            // 
            this.backShininessInput.Location = new System.Drawing.Point(96, 139);
            this.backShininessInput.Mask = "000";
            this.backShininessInput.Name = "backShininessInput";
            this.backShininessInput.Size = new System.Drawing.Size(36, 22);
            this.backShininessInput.TabIndex = 17;
            // 
            // backEmissionZInput
            // 
            this.backEmissionZInput.Location = new System.Drawing.Point(180, 112);
            this.backEmissionZInput.Mask = "0.0";
            this.backEmissionZInput.Name = "backEmissionZInput";
            this.backEmissionZInput.Size = new System.Drawing.Size(36, 22);
            this.backEmissionZInput.TabIndex = 16;
            // 
            // backEmissionYInput
            // 
            this.backEmissionYInput.Location = new System.Drawing.Point(138, 112);
            this.backEmissionYInput.Mask = "0.0";
            this.backEmissionYInput.Name = "backEmissionYInput";
            this.backEmissionYInput.Size = new System.Drawing.Size(36, 22);
            this.backEmissionYInput.TabIndex = 15;
            // 
            // backEmissionXInput
            // 
            this.backEmissionXInput.Location = new System.Drawing.Point(96, 112);
            this.backEmissionXInput.Mask = "0.0";
            this.backEmissionXInput.Name = "backEmissionXInput";
            this.backEmissionXInput.Size = new System.Drawing.Size(36, 22);
            this.backEmissionXInput.TabIndex = 14;
            // 
            // backSpecularZInput
            // 
            this.backSpecularZInput.Location = new System.Drawing.Point(180, 85);
            this.backSpecularZInput.Mask = "0.0";
            this.backSpecularZInput.Name = "backSpecularZInput";
            this.backSpecularZInput.Size = new System.Drawing.Size(36, 22);
            this.backSpecularZInput.TabIndex = 13;
            // 
            // backSpecularYInput
            // 
            this.backSpecularYInput.Location = new System.Drawing.Point(138, 85);
            this.backSpecularYInput.Mask = "0.0";
            this.backSpecularYInput.Name = "backSpecularYInput";
            this.backSpecularYInput.Size = new System.Drawing.Size(36, 22);
            this.backSpecularYInput.TabIndex = 12;
            // 
            // backSpecularXInput
            // 
            this.backSpecularXInput.Location = new System.Drawing.Point(96, 85);
            this.backSpecularXInput.Mask = "0.0";
            this.backSpecularXInput.Name = "backSpecularXInput";
            this.backSpecularXInput.Size = new System.Drawing.Size(36, 22);
            this.backSpecularXInput.TabIndex = 11;
            // 
            // backDiffuseZInput
            // 
            this.backDiffuseZInput.Location = new System.Drawing.Point(180, 58);
            this.backDiffuseZInput.Mask = "0.0";
            this.backDiffuseZInput.Name = "backDiffuseZInput";
            this.backDiffuseZInput.Size = new System.Drawing.Size(36, 22);
            this.backDiffuseZInput.TabIndex = 10;
            // 
            // backDiffuseYInput
            // 
            this.backDiffuseYInput.Location = new System.Drawing.Point(138, 58);
            this.backDiffuseYInput.Mask = "0.0";
            this.backDiffuseYInput.Name = "backDiffuseYInput";
            this.backDiffuseYInput.Size = new System.Drawing.Size(36, 22);
            this.backDiffuseYInput.TabIndex = 9;
            // 
            // backDiffuseXInput
            // 
            this.backDiffuseXInput.Location = new System.Drawing.Point(96, 58);
            this.backDiffuseXInput.Mask = "0.0";
            this.backDiffuseXInput.Name = "backDiffuseXInput";
            this.backDiffuseXInput.Size = new System.Drawing.Size(36, 22);
            this.backDiffuseXInput.TabIndex = 8;
            // 
            // backAmbientZInput
            // 
            this.backAmbientZInput.Location = new System.Drawing.Point(180, 31);
            this.backAmbientZInput.Mask = "0.0";
            this.backAmbientZInput.Name = "backAmbientZInput";
            this.backAmbientZInput.Size = new System.Drawing.Size(36, 22);
            this.backAmbientZInput.TabIndex = 7;
            // 
            // backAmbientYInput
            // 
            this.backAmbientYInput.Location = new System.Drawing.Point(138, 31);
            this.backAmbientYInput.Mask = "0.0";
            this.backAmbientYInput.Name = "backAmbientYInput";
            this.backAmbientYInput.Size = new System.Drawing.Size(36, 22);
            this.backAmbientYInput.TabIndex = 6;
            // 
            // backAmbientXInput
            // 
            this.backAmbientXInput.Location = new System.Drawing.Point(96, 31);
            this.backAmbientXInput.Mask = "0.0";
            this.backAmbientXInput.Name = "backAmbientXInput";
            this.backAmbientXInput.Size = new System.Drawing.Size(36, 22);
            this.backAmbientXInput.TabIndex = 5;
            // 
            // BackShininessLabel
            // 
            this.BackShininessLabel.AutoSize = true;
            this.BackShininessLabel.Location = new System.Drawing.Point(6, 142);
            this.BackShininessLabel.Name = "BackShininessLabel";
            this.BackShininessLabel.Size = new System.Drawing.Size(69, 17);
            this.BackShininessLabel.TabIndex = 4;
            this.BackShininessLabel.Text = "Shininess";
            // 
            // BackEmissionLabel
            // 
            this.BackEmissionLabel.AutoSize = true;
            this.BackEmissionLabel.Location = new System.Drawing.Point(6, 115);
            this.BackEmissionLabel.Name = "BackEmissionLabel";
            this.BackEmissionLabel.Size = new System.Drawing.Size(64, 17);
            this.BackEmissionLabel.TabIndex = 3;
            this.BackEmissionLabel.Text = "Emission";
            // 
            // BackSpecularLabel
            // 
            this.BackSpecularLabel.AutoSize = true;
            this.BackSpecularLabel.Location = new System.Drawing.Point(6, 88);
            this.BackSpecularLabel.Name = "BackSpecularLabel";
            this.BackSpecularLabel.Size = new System.Drawing.Size(64, 17);
            this.BackSpecularLabel.TabIndex = 2;
            this.BackSpecularLabel.Text = "Specular";
            // 
            // BackDiffuseLabel
            // 
            this.BackDiffuseLabel.AutoSize = true;
            this.BackDiffuseLabel.Location = new System.Drawing.Point(6, 61);
            this.BackDiffuseLabel.Name = "BackDiffuseLabel";
            this.BackDiffuseLabel.Size = new System.Drawing.Size(52, 17);
            this.BackDiffuseLabel.TabIndex = 1;
            this.BackDiffuseLabel.Text = "Diffuse";
            // 
            // BackAmbientLabel
            // 
            this.BackAmbientLabel.AutoSize = true;
            this.BackAmbientLabel.Location = new System.Drawing.Point(6, 34);
            this.BackAmbientLabel.Name = "BackAmbientLabel";
            this.BackAmbientLabel.Size = new System.Drawing.Size(59, 17);
            this.BackAmbientLabel.TabIndex = 0;
            this.BackAmbientLabel.Text = "Ambient";
            // 
            // ApplyButton
            // 
            this.ApplyButton.Location = new System.Drawing.Point(397, 217);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(103, 32);
            this.ApplyButton.TabIndex = 21;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = true;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // MaterialSettingsPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 257);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.backGroupBox);
            this.Controls.Add(this.frontGroupBox);
            this.Name = "MaterialSettingsPopup";
            this.Text = "Material Settings";
            this.frontGroupBox.ResumeLayout(false);
            this.frontGroupBox.PerformLayout();
            this.backGroupBox.ResumeLayout(false);
            this.backGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox frontGroupBox;
        private System.Windows.Forms.MaskedTextBox frontShininessInput;
        private System.Windows.Forms.MaskedTextBox frontEmissionZInput;
        private System.Windows.Forms.MaskedTextBox frontEmissionYInput;
        private System.Windows.Forms.MaskedTextBox frontEmissionXInput;
        private System.Windows.Forms.MaskedTextBox frontSpecularZInput;
        private System.Windows.Forms.MaskedTextBox frontSpecularYInput;
        private System.Windows.Forms.MaskedTextBox frontSpecularXInput;
        private System.Windows.Forms.MaskedTextBox frontDiffuseZInput;
        private System.Windows.Forms.MaskedTextBox frontDiffuseYInput;
        private System.Windows.Forms.MaskedTextBox frontDiffuseXInput;
        private System.Windows.Forms.MaskedTextBox frontAmbientZInput;
        private System.Windows.Forms.MaskedTextBox frontAmbientYInput;
        private System.Windows.Forms.MaskedTextBox frontAmbientXInput;
        private System.Windows.Forms.Label FrontShininessLabel;
        private System.Windows.Forms.Label FrontEmissionLabel;
        private System.Windows.Forms.Label FrontSpecularLabel;
        private System.Windows.Forms.Label FrontDiffuseLabel;
        private System.Windows.Forms.Label FrontAmbientLabel;
        private System.Windows.Forms.GroupBox backGroupBox;
        private System.Windows.Forms.MaskedTextBox backShininessInput;
        private System.Windows.Forms.MaskedTextBox backEmissionZInput;
        private System.Windows.Forms.MaskedTextBox backEmissionYInput;
        private System.Windows.Forms.MaskedTextBox backEmissionXInput;
        private System.Windows.Forms.MaskedTextBox backSpecularZInput;
        private System.Windows.Forms.MaskedTextBox backSpecularYInput;
        private System.Windows.Forms.MaskedTextBox backSpecularXInput;
        private System.Windows.Forms.MaskedTextBox backDiffuseZInput;
        private System.Windows.Forms.MaskedTextBox backDiffuseYInput;
        private System.Windows.Forms.MaskedTextBox backDiffuseXInput;
        private System.Windows.Forms.MaskedTextBox backAmbientZInput;
        private System.Windows.Forms.MaskedTextBox backAmbientYInput;
        private System.Windows.Forms.MaskedTextBox backAmbientXInput;
        private System.Windows.Forms.Label BackShininessLabel;
        private System.Windows.Forms.Label BackEmissionLabel;
        private System.Windows.Forms.Label BackSpecularLabel;
        private System.Windows.Forms.Label BackDiffuseLabel;
        private System.Windows.Forms.Label BackAmbientLabel;
        private System.Windows.Forms.Button ApplyButton;
    }
}