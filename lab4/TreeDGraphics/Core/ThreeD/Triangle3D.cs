﻿using System;
using System.Collections.Generic;
using System.Linq;
using TreeDGraphics.Helpers;

namespace TreeDGraphics.Core.ThreeD
{
    public class Triangle3D
    {
        public readonly List<Line3D> Lines;

        /// <summary>
        /// New triangle
        /// </summary>
        /// <param name="trashId">Collection of IDs of trash lines while reading lines counterclockwise.</param>
        public Triangle3D(Point3D p1, Point3D p2, Point3D p3, IEnumerable<int> trashIds)
        {
            Lines = new List<Line3D>()
            {
                new Line3D(p1, p2, trashIds?.Any(t => t == 0) ?? false),
                new Line3D(p2, p3, trashIds?.Any(t => t == 1) ?? false),
                new Line3D(p3, p1, trashIds?.Any(t => t == 2) ?? false)
            };
        }

        public Triangle3D(List<Line3D> lines)
        {
            if (lines.Count()>3)
                throw new Exception("Invalid triangle lines count!");
            Lines = new List<Line3D>(lines);
        }

        public Point3D NormalVector()
        {
            var v1 = Lines[0].DirectionVector;
            var v2 = Lines[2].Invert().DirectionVector;
            var n = v1.VectMul(v2);
            var offset = (-1)*(Lines[0].Start.X*n.X + Lines[0].Start.Y * n.Y +  Lines[0].Start.Z * n.Z);
            if (Math.Abs(offset) < 0.01)
                return n;
            return new Point3D(n.X/offset, n.Y/offset, n.Z/offset);
        }

        public Point3D CrossingPoint(Point3D pointVect, Point3D directionVect)
        {
            var n = NormalVector();
            return pointVect.VectSub(directionVect.ScalarValMul((n.ScalarMul(pointVect)+1)/n.ScalarMul(directionVect)));
        }

        public float DistanceTo(Point3D p)
        {
            var n = NormalVector();
            return Math.Abs(n.X * p.X + n.Y * p.Y + n.Z * p.Z) / n.Length();
        }

        public static float DistanceTo(Point3D n, Point3D p)
        {
            return Math.Abs(n.X * p.X + n.Y * p.Y + n.Z * p.Z + 1) / n.Length();
        }
    }
}
