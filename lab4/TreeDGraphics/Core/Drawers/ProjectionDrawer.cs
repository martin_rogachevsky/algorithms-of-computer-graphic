﻿using System;
using System.Collections.Generic;
using TreeDGraphics.Core.ThreeD;
using TreeDGraphics.Core.TwoD;

namespace TreeDGraphics.Core.Drawers
{
    public class ProjectionDrawer
    {
        private readonly IDictionary<Triangle3D, Polygon2D> _projectionBase;

        private static Triangle3D _surface;
        
        public ProjectionDrawer(IDictionary<Triangle3D, Polygon2D> projectionBase, Triangle3D projectionSurface)
        {
            _projectionBase = new Dictionary<Triangle3D, Polygon2D>(projectionBase);
            _surface = projectionSurface;
        }

        private bool IsLineBehindSurface((float, float) crossingParams, Line3D originalLine, Triangle3D originalTriangle)
        {
            var analyticalPoint = (Math.Abs(crossingParams.Item1) < 0.01 && Math.Abs(crossingParams.Item2 - 1) < 0.01)
                ? originalLine.CutLine(0, (float)0.5).End
                : (crossingParams.Item1 > 0 && Math.Abs(crossingParams.Item2 - 1) < 0.01
                    ? originalLine.CutLine(crossingParams.Item1, crossingParams.Item2).Start
                    : originalLine.CutLine(crossingParams.Item1, crossingParams.Item2).End);

            var distToLine = _surface.DistanceTo(analyticalPoint);
            var crossingPoint = originalTriangle.CrossingPoint(analyticalPoint, _surface.NormalVector());
            return _surface.DistanceTo(crossingPoint) <= distToLine;
        }

        private void BuildProjection()
        {
            foreach (var master in _projectionBase)
            {
                foreach (var slave in _projectionBase)
                {
                    for (int i = 0; i < slave.Value.Lines.Count; i++)
                    {
                        if (slave.Value.Lines[i].IsTrash)
                            continue;
                      
                        var crossingPoints = CyrusBeck.CrossByLine(master.Value, slave.Value.Lines[i]);
                        if (crossingPoints.Item1 < 0.05 && crossingPoints.Item2 < 0.05)
                            continue;
                        var lineBehind = IsLineBehindSurface(crossingPoints, slave.Key.Lines[i], master.Key);
                        if (lineBehind)
                        {
                            slave.Value.Lines[i].HiddenParts.Add(crossingPoints);
                        }
                    }
                }   
            }
        }

        public void Draw(IntPtr renderer, bool isRerender, bool isLineHidden, bool isLineShown)
        {
            if (isRerender)
                BuildProjection();
            foreach (var polygon2D in _projectionBase.Values)
            {
                foreach (var line in polygon2D.Lines)
                {
                    if (!line.IsTrash || isLineHidden)
                        line.Draw(renderer, isLineShown);
                }
            }
        }
    }
}
