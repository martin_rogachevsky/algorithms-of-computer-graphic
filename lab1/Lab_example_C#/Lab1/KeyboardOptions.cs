﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDL2;

namespace Lab1
{
    internal class KeyboardOptions
    {
        public int MoveStep { get; private set; }

        public double RotateStep { get; private set; }

        public double InitialRotation { get; private set; }

        public KeyboardOptions(int moveStep, double rotateStep, double initialRotation)
        {
            this.MoveStep = moveStep;
            this.RotateStep = rotateStep;
            this.InitialRotation = initialRotation;
        }

        public int OnMove(SDL.SDL_Keycode keycode)
        {
            switch (keycode)
            {
                case SDL.SDL_Keycode.SDLK_DOWN:
                    return MoveStep;
                case SDL.SDL_Keycode.SDLK_UP:
                    return (-1) * MoveStep;

                default:
                    return MoveStep;
            }
        }

        public double OnRotate(SDL.SDL_Keycode keycode)
        {
            switch (keycode)
            {
                case SDL.SDL_Keycode.SDLK_LEFT:
                    return RotateStep;
                case SDL.SDL_Keycode.SDLK_RIGHT:
                    return (-1) * RotateStep;

                default:
                    return RotateStep;
            }
        }
    }
}
