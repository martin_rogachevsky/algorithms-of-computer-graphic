﻿using Shapes.Forms;

namespace Shapes
{
    internal static class Program
    {
        private static void Main()
        {
            new RectangleForm();
            new TriangleForm();
        }
    }
}