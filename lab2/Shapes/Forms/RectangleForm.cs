﻿using Shapes.Figures;
using Shapes.GeometryElements;

namespace Shapes.Forms
{
    internal class RectangleForm : AbstractForm
    {
        protected override RotatableShape CreateShape()
        {
            return new Rectangle(new Point(185, 100), new Size(400, 400), 1);
        }
    }
}