﻿using System;
using System.Collections.Generic;
using System.Drawing;
using TreeDGraphics.Core.TwoD;

namespace TreeDGraphics.Helpers
{
    public static class GraphicHelper
    {
        public static IEnumerable<PointF> GetDottedLines(this Line2D line, int lengthOne = 4, int countPixelBetweenLines = 9)
        {
            var subLines = new List<PointF>();
            float x1 = line.Start.X, x2 = line.End.X, y1 = line.Start.Y, y2 = line.End.Y;
            var dx = (int)Math.Abs(x2 - x1);
            var dy = (int)Math.Abs(y2 - y1);
            var sx = x2 >= x1 ? 1 : -1;
            var sy = y2 >= y1 ? 1 : -1;
            var lengthFullLine = lengthOne + countPixelBetweenLines;
            var numberPoint = 0;
            if (dy <= dx)
            {
                var d = (dy << 1) - dx;
                var d1 = dy << 1;
                var d2 = (dy - dx) << 1;
                subLines.Add(new PointF(x1, y1));
                for (int x = (int)(x1 + sx), y = (int)y1, i = 1; i <= dx; i++, x += sx)
                {
                    if (numberPoint == lengthFullLine)
                    {
                        numberPoint = 1;
                    }
                    else
                    {
                        numberPoint += 1;
                    }

                    if (d > 0)
                    {
                        d += d2;
                        y += sy;
                    }
                    else
                        d += d1;

                    if (numberPoint <= lengthOne)
                        subLines.Add(new Point(x, y));
                }
            }
            else
            {
                var d = (dx << 1) - dy;
                var d1 = dx << 1;
                var d2 = (dx - dy) << 1;
                subLines.Add(new PointF(x1, y1));
                for (int x = (int)x1, y = (int)(y1 + sy), i = 1; i <= dy; i++, y += sy)
                {
                    if (numberPoint == lengthFullLine)
                    {
                        numberPoint = 1;
                    }
                    else
                    {
                        numberPoint += 1;
                    }

                    if (d > 0)
                    {
                        d += d2;
                        x += sx;
                    }
                    else
                        d += d1;

                    if (numberPoint <= lengthOne)
                        subLines.Add(new Point(x, y));
                }
            }

            return subLines;
        }
    }
}
