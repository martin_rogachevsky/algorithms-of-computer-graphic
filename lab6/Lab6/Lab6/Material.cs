﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab6
{
    public class Material
    {
        public FaceProperties FrontMaterialProperties { get; set; }
        public FaceProperties BackMaterialProperties { get; set; }

        public Material()
        {
            FrontMaterialProperties = new FaceProperties();
            BackMaterialProperties = new FaceProperties();
        }
    }

    public class FaceProperties
    {
        public Options Ambient { get; set; }
        public Options Diffuse { get; set; }
        public Options Specular { get; set; }
        public Options Emission { get; set; }
        public Options Shininess { get; set; }

        public FaceProperties()
        {
            Ambient = new Options
            {
                X = 0.2f,
                Y = 0.2f,
                Z = 0.2f,
                W = 1.0f
            };

            Diffuse = new Options
            {
                X = 0.8f,
                Y = 0.8f,
                Z = 0.8f,
                W = 1.0f
            };

            Specular = new Options
            {
                X = 0.0f,
                Y = 0.0f,
                Z = 0.0f,
                W = 1.0f
            };

            Emission = new Options
            {
                X = 0.0f,
                Y = 0.0f,
                Z = 0.0f,
                W = 1.0f
            };

            Shininess = new Options
            {
                W = 0
            };
        }
    }

    public class Options
    {
        public float? X { get; set; }
        public float? Y { get; set; }
        public float? Z { get; set; }
        public float? W { get; set; }
    }
}
