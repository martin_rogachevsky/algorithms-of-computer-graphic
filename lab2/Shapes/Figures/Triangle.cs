﻿using System;
using Shapes.GeometryElements;

namespace Shapes.Figures
{
    internal class Triangle : RotatableShape
    {
        public Triangle(Point leftVertex, int sizeSide, int rotationCoefficient) : base(rotationCoefficient)
        {
            Points.Add(leftVertex);
            Points.Add(new Point(leftVertex.X + sizeSide / (double) 2, leftVertex.Y + Math.Sqrt(3) * sizeSide / 2));
            Points.Add(new Point(leftVertex.X + sizeSide, leftVertex.Y));
        }
    }
}