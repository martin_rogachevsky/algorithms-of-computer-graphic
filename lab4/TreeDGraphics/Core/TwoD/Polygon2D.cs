using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using SDL2;

namespace TreeDGraphics.Core.TwoD
{
    public class Polygon2D
    {
        public readonly List<Line2D> Lines = new List<Line2D>();
        public readonly bool IsOrdered;

        public Polygon2D(bool isOrdered)
        {
            IsOrdered = isOrdered;
        }

        public Polygon2D(SDL.SDL_Rect rect)
        {
            IsOrdered = true;
            Lines.Add(new Line2D(rect.x, rect.y, rect.x, rect.y + rect.h));
            Lines.Add(new Line2D(rect.x, rect.y + rect.h, rect.x + rect.w, rect.y + rect.h));
            Lines.Add(new Line2D(rect.x + rect.w, rect.y + rect.h, rect.x + rect.w, rect.y));
            Lines.Add(new Line2D(rect.x + rect.w, rect.y, rect.x, rect.y));
        }

        public Polygon2D(IEnumerable<Line2D> lines, bool isOrdered)
        {
            IsOrdered = isOrdered;
            Lines = lines.ToList();
        }

        public void Offset(int x, int y)
        {
            foreach (var line in Lines)
            {
                line.Start = new PointF(line.Start.X + x, line.Start.Y + y);
                line.End = new PointF(line.End.X + x, line.End.Y + y);
            }
        }

        public void CleanTrash() => Lines.RemoveAll(l => l.IsTrash);

        public void Rotate(double deg)
        {
            deg = deg * 180 / Math.PI;
            double x = 0, y = 0;
            Lines.ForEach(l => x = x + l.Start.X + l.End.X);
            Lines.ForEach(l => y = y + l.Start.Y + l.End.Y);
            x = x / (Lines.Count * 2);
            y = y / (Lines.Count * 2);

            foreach (var line in Lines)
            {
                line.Start =
                    new Point((int) (x + (line.Start.X - x) * Math.Cos(deg) - (line.Start.Y - y) * Math.Sin(deg)),
                        (int) (y + (line.Start.X - x) * Math.Sin(deg) + (line.Start.Y - y) * Math.Cos(deg)));
                line.End = new Point((int) (x + (line.End.X - x) * Math.Cos(deg) - (line.End.Y - y) * Math.Sin(deg)),
                    (int) (y + (line.End.X - x) * Math.Sin(deg) + (line.End.Y - y) * Math.Cos(deg)));
            }
        }
    }
}