using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using SDL2;

namespace ShapesCrossing
{
    public class Line
    {
        public Point Start { get; set; }
        public Point End { get; set; }

        public Line(int sx, int sy, int ex, int ey)
        {
            Start = new Point(sx, sy);
            End = new Point(ex, ey);
        }

        public void Draw(IntPtr renderer, bool isDotted = false)
        {
            if (isDotted)
            {
                var points = GetDottedLines();
                foreach (var point in points)
                {
                    SDL.SDL_RenderDrawPoint(renderer, point.X, point.Y);
                }
            }
            else
            {
                SDL.SDL_RenderDrawLine(renderer, Start.X, Start.Y, End.X, End.Y);
            }
        }

        private IEnumerable<Point> GetDottedLines(int lengthOne = 7, int countPixelBetweenLines = 3)
        {
            var subLines = new List<Point>();
            int x1 = Start.X, x2 = End.X, y1 = Start.Y, y2 = End.Y;
            var dx = (int) Math.Abs(x2 - x1);
            var dy = (int) Math.Abs(y2 - y1);
            var sx = x2 >= x1 ? 1 : -1;
            var sy = y2 >= y1 ? 1 : -1;
            var lengthFullLine = lengthOne + countPixelBetweenLines;
            var numberPoint = 0;
            if (dy <= dx)
            {
                var d = (dy << 1) - dx;
                var d1 = dy << 1;
                var d2 = (dy - dx) << 1;
                subLines.Add(new Point(x1, y1));
                for (int x = x1 + sx, y = y1, i = 1; i <= dx; i++, x += sx)
                {
                    if (numberPoint == lengthFullLine)
                    {
                        numberPoint = 1;
                    }
                    else
                    {
                        numberPoint += 1;
                    }

                    if (d > 0)
                    {
                        d += d2;
                        y += sy;
                    }
                    else
                        d += d1;

                    if (numberPoint <= lengthOne)
                        subLines.Add(new Point(x, y));
                }
            }
            else
            {
                var d = (dx << 1) - dy;
                var d1 = dx << 1;
                var d2 = (dx - dy) << 1;
                subLines.Add(new Point(x1, y1));
                for (int x = x1, y = y1 + sy, i = 1; i <= dy; i++, y += sy)
                {
                    if (numberPoint == lengthFullLine)
                    {
                        numberPoint = 1;
                    }
                    else
                    {
                        numberPoint += 1;
                    }

                    if (d > 0)
                    {
                        d += d2;
                        x += sx;
                    }
                    else
                        d += d1;

                    if (numberPoint <= lengthOne)
                        subLines.Add(new Point(x, y));
                }
            }

            return subLines;
        }

        public PointF NormalVect() =>
            new PointF(Start.Y - End.Y, End.X - Start.X);

        public PointF DirectVect() =>
            new PointF(End.X - Start.X, End.Y - Start.Y);

        public Line Invert() =>
            new Line(End.X, End.Y, Start.X, Start.Y);

        public Line CutLine(float tStart, float tEnd)
        {
            var vect1 = ShapesJoiner.VectAdd(Start, ShapesJoiner.ScalarValMul(DirectVect(), tStart));
            var vect2 = ShapesJoiner.VectAdd(Start, ShapesJoiner.ScalarValMul(DirectVect(), tEnd));
            return new Line((int) vect1.X, (int) vect1.Y, (int) vect2.X, (int) vect2.Y);
        }
    }

    public class Polygon
    {
        public readonly List<Line> Lines = new List<Line>();
        public readonly bool IsOrdered;

        public Polygon(bool isOrdered)
        {
            IsOrdered = isOrdered;
        }

        public Polygon(SDL.SDL_Rect rect)
        {
            IsOrdered = true;
            Lines.Add(new Line(rect.x, rect.y, rect.x, rect.y + rect.h));
            Lines.Add(new Line(rect.x, rect.y + rect.h, rect.x + rect.w, rect.y + rect.h));
            Lines.Add(new Line(rect.x + rect.w, rect.y + rect.h, rect.x + rect.w, rect.y));
            Lines.Add(new Line(rect.x + rect.w, rect.y, rect.x, rect.y));
        }

        public Polygon(IEnumerable<Line> lines, bool isOrdered = false)
        {
            IsOrdered = isOrdered;
            Lines = lines.ToList();
        }

        public void Offset(int x, int y)
        {
            foreach (var line in Lines)
            {
                line.Start = new Point(line.Start.X + x, line.Start.Y + y);
                line.End = new Point(line.End.X + x, line.End.Y + y);
            }
        }

        public void Rotate(double deg)
        {
            deg = deg *180 / Math.PI;
            double x = 0, y = 0;
            Lines.ForEach(l => x = x + l.Start.X + l.End.X);
            Lines.ForEach(l => y = y + l.Start.Y + l.End.Y);
            x = x / (Lines.Count * 2);
            y = y / (Lines.Count * 2);

            foreach (var line in Lines)
            {
                line.Start = new Point((int)(x + (line.Start.X - x)*Math.Cos(deg) - (line.Start.Y - y)*Math.Sin(deg)), (int)(y + (line.Start.X - x) * Math.Sin(deg) + (line.Start.Y - y) * Math.Cos(deg)));
                line.End = new Point((int)(x + (line.End.X - x) * Math.Cos(deg) - (line.End.Y - y) * Math.Sin(deg)), (int)(y + (line.End.X - x) * Math.Sin(deg) + (line.End.Y - y) * Math.Cos(deg)));
            }
        }

        public void Draw(IntPtr renderer, bool isDotted = false)
        {
            foreach (var line in Lines)
            {
                line.Draw(renderer, isDotted);
            }
        }
    }

    public class ShapesJoiner
    {
        public static PointF VectAdd(PointF v1, PointF v2)
        {
            return new PointF(v1.X + v2.X, v1.Y + v2.Y);
        }

        public static float ScalarMul(PointF v1, PointF v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y;
        }

        public static PointF ScalarValMul(PointF v1, float val)
        {
            return new PointF(v1.X * val, v1.Y * val);
        }

        public static PointF VectSub(PointF v1, PointF v2)
        {
            return new PointF(v1.X - v2.X, v1.Y - v2.Y);
        }

        public static bool IsLeft(Line baseLine, Line observingLine)
        {
            var d = baseLine.DirectVect();
            var s = VectSub(observingLine.Start, baseLine.Start);
            return d.X * s.Y - d.Y * s.X >= 0;
        }

        public static float JoinPont(Line crossingLine, Line edgeLine)
        {
            var edgeNormalVect = edgeLine.NormalVect();

            var t = -1 * ScalarMul(edgeNormalVect, VectSub(crossingLine.Start, edgeLine.Start)) /
                    ScalarMul(edgeNormalVect, crossingLine.DirectVect());

            if (Single.IsNaN(t))
                return 0;

            return t;
        }

        public static Polygon Join(Polygon primaryPolygon, Polygon secondaryPolygon, bool isInside = true)
        {
            var result = new Polygon(false);

            foreach (var line in secondaryPolygon.Lines)
            {
                float tStart = 0;
                float tEnd = 1;
                var isJoining = true;

                foreach (var edge in primaryPolygon.Lines)
                {
                    switch (Math.Sign(ScalarMul(line.DirectVect(), edge.NormalVect())))
                    {
                        case -1:
                            tStart = Math.Max(tStart, JoinPont(line, edge));
                            break;
                        case +1:
                            tEnd = Math.Min(tEnd, JoinPont(line, edge));
                            break;
                        case 0:
                            if (IsLeft(edge, line))
                                isJoining = false;
                            break;
                    }

                    if (!isJoining) break;
                }

                if (isInside && isJoining && tStart < tEnd)
                    result.Lines.Add(line.CutLine(tStart, tEnd));

                if (!isInside)
                {
                    if (!isJoining || tStart > tEnd || tStart >= 1 || tEnd <= 0)
                    {
                        result.Lines.Add(line);
                    }
                    else if (tStart < tEnd)
                    {
                        result.Lines.Add(line.CutLine(0, tStart));
                        result.Lines.Add(line.CutLine(tEnd, 1));
                    }
                }
            }

            return result;
        }
    }
}