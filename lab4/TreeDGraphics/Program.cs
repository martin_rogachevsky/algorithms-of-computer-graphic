﻿using System;
using System.Threading;
using SDL2;
using TreeDGraphics.Core;
using TreeDGraphics.Core.Drawers;
using TreeDGraphics.Core.ThreeD;
using TreeDGraphics.Core.Transformers;
using TreeDGraphics.Helpers;

namespace TreeDGraphics
{
    internal static class Program
    {
        private const int ScreenWidth = 640;
        private const int ScreenHeight = 480;
        private static ProjectionDrawer _drawer;
        private static Point3D _rotationAxis;
        private static Point3D _radiusVectorRotation;
        private static Object3D _drawingObject;
        private static Point3D _coordinatesSystemPos;
        private static int _rotatingAngle;
        private static Point3D _rotationAxisObject;
        private static Mode _mode;
        private static int ro, phi, theta, d;
        private static bool _isLinesHidden;
        private static bool _isLinesShown;

        private static void Main()
        {
            while (true)
            {
                _isLinesHidden = false;
                _isLinesShown = true;
                Console.WriteLine("Select operation mode, default(d), perspective(p)");
                var mode = Console.ReadLine();
                int H;

                if (mode.Trim().Equals("d"))
                {
                    _mode = Mode.DEFAULT;
                }

                if (mode.Trim().Equals("p"))
                {
                    _mode = Mode.PERSPECTIVE;
                    Console.WriteLine("Enter p");
                    ro = Int32.Parse(Console.ReadLine().Trim());
                    Console.WriteLine("Enter phi");
                    phi = Int32.Parse(Console.ReadLine().Trim());
                    Console.WriteLine("Enter theta");
                    theta = Int32.Parse(Console.ReadLine().Trim());
                    Console.WriteLine("Enter d");
                    d = Int32.Parse(Console.ReadLine().Trim());
                }

                Console.WriteLine("Enter H");
                H = Int32.Parse(Console.ReadLine().Trim());

                _coordinatesSystemPos = new Point3D(200, 200, 200);
                //_drawingObject = new Object3D(new[]
                //{
                //    new Triangle3D(new Point3D(90, 80, 10), new Point3D(20, 30, 30), new Point3D(110, 30, 30), null),
                //    new Triangle3D(new Point3D(20, 30, 120), new Point3D(90, 80, 100), new Point3D(110, 30, 120), null),
                //    new Triangle3D(new Point3D(90, 80, 100), new Point3D(90, 80, 10), new Point3D(110, 30, 120),
                //        new[] {1, 2}),
                //    new Triangle3D(new Point3D(110, 30, 30), new Point3D(110, 30, 120), new Point3D(90, 80, 10),
                //        new[] {1, 2}),
                //    new Triangle3D(new Point3D(20, 30, 120), new Point3D(110, 30, 120), new Point3D(110, 30, 30),
                //        new[] {0, 1, 2}),
                //    new Triangle3D(new Point3D(20, 30, 120), new Point3D(110, 30, 30), new Point3D(20, 30, 30),
                //        new[] {0, 1}),
                //    new Triangle3D(new Point3D(20, 30, 120), new Point3D(20, 30, 30), new Point3D(90, 80, 10),
                //        new[] {0, 1, 2}),
                //    new Triangle3D(new Point3D(20, 30, 120), new Point3D(90, 80, 10), new Point3D(90, 80, 100),
                //        new[] {0, 1, 2}),
                //});

                float cosOut = (float) (Math.Cos(30.0 / 180 * Math.PI) * 4.5 * H);
                float coIn1 = (float) 0.75 * H;
                float coIn2 = (float) (Math.Cos(30.0 / 180 * Math.PI) * 3 * H);

                _radiusVectorRotation = new Point3D((float) (2.25 * H), cosOut, 0);
                _rotationAxis = new Point3D(1, 0, 0);
                _rotationAxisObject =
                    new Point3D(0, (float) (0.75 * H - 4.5 * H * Math.Cos(30 / (float) 180 * Math.PI)), H);
                _drawingObject = new Object3D(new[]
                {
                    new Triangle3D(new Point3D(0, 0, 0), new Point3D(0, 0, H), new Point3D((float) 4.5 * H, 0, 0),
                        new[] {1}),
                    new Triangle3D(new Point3D(0, 0, H), new Point3D((float) 4.5 * H, 0, H),
                        new Point3D((float) 4.5 * H, 0, 0), new[] {2}),
                    new Triangle3D(new Point3D(0, 0, H), new Point3D(0, 0, 0), new Point3D((float) 2.25 * H, cosOut, H),
                        new[] {0, 1}),
                    new Triangle3D(new Point3D((float) 2.25 * H, cosOut, H), new Point3D(0, 0, 0),
                        new Point3D((float) 2.25 * H, cosOut, 0), new[] {0}),
                    new Triangle3D(new Point3D((float) 2.25 * H, cosOut, H), new Point3D((float) 2.25 * H, cosOut, 0),
                        new Point3D((float) 4.5 * H, 0, H), new[] {0, 1}),
                    new Triangle3D(new Point3D((float) 2.25 * H, cosOut, 0), new Point3D((float) 4.5 * H, 0, 0),
                        new Point3D((float) 4.5 * H, 0, H), new[] {1, 2}),

                    new Triangle3D(new Point3D(0, 0, H), new Point3D((float) 1.5 * H, coIn1, H),
                        new Point3D((float) 4.5 * H, 0, H), new[] {0, 1, 2}),
                    new Triangle3D(new Point3D((float) 4.5 * H, 0, H), new Point3D(3 * H, coIn1, H),
                        new Point3D((float) 2.25 * H, cosOut, H), new[] {0, 1, 2}),
                    new Triangle3D(new Point3D(0, 0, H), new Point3D((float) 2.25 * H, cosOut, H),
                        new Point3D((float) 2.25 * H, coIn2, H), new[] {0, 1, 2}),
                    new Triangle3D(new Point3D(0, 0, H), new Point3D((float) 2.25 * H, coIn2, H),
                        new Point3D((float) 1.5 * H, coIn1, H), new[] {0, 2}),
                    new Triangle3D(new Point3D((float) 4.5 * H, 0, H), new Point3D((float) 1.5 * H, coIn1, H),
                        new Point3D(3 * H, coIn1, H), new[] {0, 2}),
                    new Triangle3D(new Point3D((float) 2.25 * H, cosOut, H), new Point3D((float) 2.25 * H, coIn2, H),
                        new Point3D(3 * H, coIn1, H), new[] {0, 2}),

                    new Triangle3D(new Point3D((float) 1.5 * H, coIn1, 0), new Point3D(0, 0, 0),
                        new Point3D((float) 4.5 * H, 0, 0), new[] {0, 1, 2}),
                    new Triangle3D(new Point3D(3 * H, coIn1, 0), new Point3D((float) 4.5 * H, 0, 0),
                        new Point3D((float) 2.25 * H, cosOut, 0), new[] {0, 1, 2}),
                    new Triangle3D(new Point3D((float) 2.25 * H, cosOut, 0), new Point3D(0, 0, 0),
                        new Point3D((float) 2.25 * H, coIn2, 0), new[] {0, 1, 2}),
                    new Triangle3D(new Point3D((float) 2.25 * H, coIn2, 0), new Point3D(0, 0, 0),
                        new Point3D((float) 1.5 * H, coIn1, 0), new[] {0, 1}),
                    new Triangle3D(new Point3D((float) 1.5 * H, coIn1, 0), new Point3D((float) 4.5 * H, 0, 0),
                        new Point3D(3 * H, coIn1, 0), new[] {0, 1}),
                    new Triangle3D(new Point3D((float) 2.25 * H, cosOut, 0), new Point3D((float) 2.25 * H, coIn2, 0),
                        new Point3D(3 * H, coIn1, 0), new[] {0, 2}),

                    new Triangle3D(new Point3D((float) 1.5 * H, coIn1, H), new Point3D((float) 1.5 * H, coIn1, 0),
                        new Point3D(3 * H, coIn1, 0), new[] {1, 2}),
                    new Triangle3D(new Point3D(3 * H, coIn1, H), new Point3D((float) 1.5 * H, coIn1, H),
                        new Point3D(3 * H, coIn1, 0), new[] {0, 1}),
                    new Triangle3D(new Point3D((float) 2.25 * H, coIn2, H), new Point3D((float) 1.5 * H, coIn1, H),
                        new Point3D((float) 1.5 * H, coIn1, 0), new[] {0, 2}),
                    new Triangle3D(new Point3D((float) 1.5 * H, coIn1, 0), new Point3D((float) 2.25 * H, coIn2, H),
                        new Point3D((float) 2.25 * H, coIn2, 0), new[] {0, 2}),
                    new Triangle3D(new Point3D((float) 2.25 * H, coIn2, 0), new Point3D((float) 2.25 * H, coIn2, H),
                        new Point3D(3 * H, coIn1, H), new[] {1, 2}),
                    new Triangle3D(new Point3D(3 * H, coIn1, 0), new Point3D((float) 2.25 * H, coIn2, 0),
                        new Point3D(3 * H, coIn1, H), new[] {0, 1}),
                });

                var offset2 = _drawingObject.MoveToCoordinateSystemStart();
                _radiusVectorRotation.Move(offset2);

                var thread = new Thread(() =>
                {
                    SDL.SDL_Init(SDL.SDL_INIT_EVERYTHING);
                    var wnd = SDL.SDL_CreateWindow("3DGraphics", 100, 100, ScreenWidth, ScreenHeight,
                        SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN);

                    var renderer = SDL.SDL_CreateRenderer(wnd, -1, SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED);

                    var quit = false;
                    var bWasDown = false;
                    while (!quit)
                    {
                        SDL.SDL_SetRenderDrawColor(renderer, 235, 235, 235, 0);
                        SDL.SDL_RenderClear(renderer);

                        SDL.SDL_PollEvent(out var sdlEvent);
                        switch (sdlEvent.type)
                        {
                            case SDL.SDL_EventType.SDL_QUIT:
                            {
                                quit = true;
                                break;
                            }
                            case SDL.SDL_EventType.SDL_KEYDOWN:
                            {
                                Point3D offset;
                                Point3D endAxis;
                                Line3D newAxis;
                                switch (sdlEvent.key.keysym.sym)
                                {
                                    case SDL.SDL_Keycode.SDLK_x:
                                        _rotationAxis = new Point3D(1, 0, 0);
//                                        _radiusVectorRotation = new Point3D(0, 0, 0);
                                        _rotatingAngle = 10;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_y:
                                        _rotationAxis = new Point3D(0, 1, 0);
//                                        _radiusVectorRotation = new Point3D(0, 0, 0);
                                        _rotatingAngle = 10;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_z:
                                        _rotationAxis = new Point3D(0, 0, 1);
//                                        _radiusVectorRotation = new Point3D(0, 0, 0);
                                        _rotatingAngle = 10;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_b:
//                                        if (bWasDown)
//                                        {
//                                            Point3D endAxis = new Point3D(
//                                                _rotationAxis.X + _radiusVectorRotation.X,
//                                                _rotationAxis.Y + _radiusVectorRotation.Y,
//                                                _rotationAxis.Z + _radiusVectorRotation.Z
//                                            );
//                                            Line3D newAxis = RotationTransformer.RotateAxis(
//                                                new Line3D(_radiusVectorRotation, endAxis),
//                                                new Point3D(1, 0, 0),
//                                                360 / (float) 180 * Math.PI);
////                                            
//                                            _drawingObject = new Object3D(RotationTransformer.RotateObject(
//                                                _drawingObject,
//                                                VectorHelper.UnitaryVector(new Point3D(1, 0, 0)),
//                                                360 / (float) 180 * Math.PI));

//                                            _radiusVectorRotation = newAxis.Start;
//                                            _rotationAxis = new Point3D(
//                                                newAxis.End.X - newAxis.Start.X,
//                                                newAxis.End.Y - newAxis.Start.Y,
//                                                newAxis.End.Z - newAxis.Start.Z
//                                            );
//                                        }
//                                        else
//                                        {
//                                            _rotationAxisObject = new Point3D(0, (float) (0.75*H-4.5*H*Math.Cos(30/ (float) 180 * Math.PI)), H);                                           
//                                        }

//                                        bWasDown = true;
                                        _rotatingAngle = 3;
                                        _drawingObject = new Object3D(RotationTransformer.RotateObject(_drawingObject,
                                            VectorHelper.UnitaryVector(_rotationAxisObject),
                                            _rotatingAngle / (float) 180 * Math.PI, _radiusVectorRotation));
                                        _drawingObject.NeedToRerender = true;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_n:
//                                        if (bWasDown)
//                                        {
                                        endAxis = new Point3D(
                                            _rotationAxisObject.X + _radiusVectorRotation.X,
                                            _rotationAxisObject.Y + _radiusVectorRotation.Y,
                                            _rotationAxisObject.Z + _radiusVectorRotation.Z
                                        );
                                        newAxis = RotationTransformer.RotateAxis(
                                            new Line3D(_radiusVectorRotation, endAxis),
                                            new Point3D(1, 0, 0),
                                            _rotatingAngle / (float) 180 * Math.PI);

                                        _drawingObject = new Object3D(RotationTransformer.RotateObject(
                                            _drawingObject,
                                            VectorHelper.UnitaryVector(new Point3D(1, 0, 0)),
                                            _rotatingAngle / (float) 180 * Math.PI));

                                        _radiusVectorRotation = newAxis.Start;
                                        _rotationAxisObject = new Point3D(
                                            newAxis.End.X - newAxis.Start.X,
                                            newAxis.End.Y - newAxis.Start.Y,
                                            newAxis.End.Z - newAxis.Start.Z
                                        );
//                                        }
//                                        else
//                                        {
//                                            _rotationAxisObject = new Point3D(0, (float) (0.75*H-4.5*H*Math.Cos(30/ (float) 180 * Math.PI)), H);                                           
//                                        }

//                                        bWasDown = true;
                                        _rotatingAngle = 3;
                                        _drawingObject = new Object3D(RotationTransformer.RotateObject(_drawingObject,
                                            VectorHelper.UnitaryVector(_rotationAxisObject),
                                            _rotatingAngle / (float) 180 * Math.PI, _radiusVectorRotation));
                                        _drawingObject.NeedToRerender = true;
                                        break;
                                        case SDL.SDL_Keycode.SDLK_h:
                                            _isLinesHidden = !_isLinesHidden;
                                            break;
                                    case SDL.SDL_Keycode.SDLK_s:
                                        _isLinesShown = !_isLinesShown;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_q:
                                        phi += 5;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_e:
                                        theta += 5;
                                        break;
                                        case SDL.SDL_Keycode.SDLK_r:
                                        endAxis = new Point3D(
                                            _rotationAxisObject.X + _radiusVectorRotation.X,
                                            _rotationAxisObject.Y + _radiusVectorRotation.Y,
                                            _rotationAxisObject.Z + _radiusVectorRotation.Z
                                        );
                                        newAxis = RotationTransformer.RotateAxis(
                                            new Line3D(_radiusVectorRotation, endAxis),
                                            _rotationAxis,
                                            _rotatingAngle / (float) 180 * Math.PI);

                                        _radiusVectorRotation = newAxis.Start;
                                        _rotationAxisObject = new Point3D(
                                            newAxis.End.X - newAxis.Start.X,
                                            newAxis.End.Y - newAxis.Start.Y,
                                            newAxis.End.Z - newAxis.Start.Z
                                        );
                                        _drawingObject = new Object3D(RotationTransformer.RotateObject(_drawingObject,
                                            _rotationAxis, _rotatingAngle / (float) 180 * Math.PI));
                                        _drawingObject.NeedToRerender = true;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_m:
                                        endAxis = new Point3D(
                                            _rotationAxisObject.X + _radiusVectorRotation.X,
                                            _rotationAxisObject.Y + _radiusVectorRotation.Y,
                                            _rotationAxisObject.Z + _radiusVectorRotation.Z
                                        );
                                        newAxis = RotationTransformer.RotateAxis(
                                            new Line3D(_radiusVectorRotation, endAxis),
                                            _rotationAxis,
                                            -_rotatingAngle / (float) 180 * Math.PI);

                                        _radiusVectorRotation = newAxis.Start;
                                        _rotationAxisObject = new Point3D(
                                            newAxis.End.X - newAxis.Start.X,
                                            newAxis.End.Y - newAxis.Start.Y,
                                            newAxis.End.Z - newAxis.Start.Z
                                        );
                                        _drawingObject = new Object3D(RotationTransformer.RotateObject(_drawingObject,
                                            _rotationAxis, -_rotatingAngle / (float) 180 * Math.PI));
                                        _drawingObject.NeedToRerender = true;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_DOWN:
                                        offset = new Point3D(0, 0, 5);
                                        _drawingObject.Move(offset);
                                        _radiusVectorRotation.Move(offset);
                                        _drawingObject.NeedToRerender = true;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_UP:
                                        offset = new Point3D(0, 0, -5);
                                        _drawingObject.Move(offset);
                                        _radiusVectorRotation.Move(offset);
                                        _drawingObject.NeedToRerender = true;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_LEFT:
                                        offset = new Point3D(-5, 0, 0);
                                        _drawingObject.Move(offset);
                                        _radiusVectorRotation.Move(offset);
                                        _drawingObject.NeedToRerender = true;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_RIGHT:
                                        offset = new Point3D(5, 0, 0);
                                        _drawingObject.Move(offset);
                                        _radiusVectorRotation.Move(offset);
                                        _drawingObject.NeedToRerender = true;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_a:
                                        offset = _drawingObject.MoveToCoordinateSystemStart();
                                        _radiusVectorRotation.Move(offset);
                                        _drawingObject.NeedToRerender = true;
                                        break;
                                }

                                break;
                            }
                            case SDL.SDL_EventType.SDL_MOUSEMOTION:
                            {
                                break;
                            }
                        }

                        Draw(renderer);
                        Thread.Sleep(10);
                    }

                    SDL.SDL_DestroyRenderer(renderer);
                    SDL.SDL_DestroyWindow(wnd);
                    SDL.SDL_Quit();
                });
                thread.Start();
                thread.Join();
            }
        }

        private static void Draw(IntPtr renderer)
        {
            switch (_mode)
            {
                case Mode.DEFAULT:
                    DrawAxisRotation(renderer, _rotationAxisObject, _radiusVectorRotation);
                    DrawCoordinatesSystem(renderer);
                    SDL.SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
                    _drawingObject.Draw(renderer, _coordinatesSystemPos, _isLinesHidden, _isLinesShown);
                    break;
                case Mode.PERSPECTIVE:
                    SDL.SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
                    _drawingObject.DrawInPerspective(renderer, ro, phi, theta, d);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            SDL.SDL_RenderPresent(renderer);
        }

        private static void DrawCoordinatesSystem(IntPtr renderer)
        {
            SDL.SDL_SetRenderDrawColor(renderer, 240, 0, 0, 0);
            SDL.SDL_RenderDrawLine(renderer, (int) _coordinatesSystemPos.X, (int) _coordinatesSystemPos.Y,
                (int) _coordinatesSystemPos.X + 50, (int) _coordinatesSystemPos.Y);
            SDL.SDL_SetRenderDrawColor(renderer, 0, 240, 0, 0);
            SDL.SDL_RenderDrawLine(renderer, (int) _coordinatesSystemPos.X, (int) _coordinatesSystemPos.Y,
                (int) _coordinatesSystemPos.X, (int) _coordinatesSystemPos.Y + 50);
            SDL.SDL_SetRenderDrawColor(renderer, 0, 0, 240, 0);
            SDL.SDL_RenderDrawLine(renderer, (int) _coordinatesSystemPos.X, (int) _coordinatesSystemPos.Y,
                (int) _coordinatesSystemPos.X - 50, (int) _coordinatesSystemPos.Y - 50);
        }

        private static void DrawAxisRotation(IntPtr renderer, Point3D axisRotation, Point3D radiusVector)
        {
            SDL.SDL_SetRenderDrawColor(renderer, 0, 0, 240, 0);
            SDL.SDL_RenderDrawLine(renderer,
                (int) (radiusVector.X + _coordinatesSystemPos.X),
                (int) (radiusVector.Z + _coordinatesSystemPos.Z),
                (int) (radiusVector.X + axisRotation.X + _coordinatesSystemPos.X),
                (int) (radiusVector.Z + axisRotation.Z + _coordinatesSystemPos.Z));
        }
    }
}