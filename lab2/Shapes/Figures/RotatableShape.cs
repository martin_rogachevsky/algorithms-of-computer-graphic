﻿using System.Collections.Generic;
using Shapes.GeometryElements;

namespace Shapes.Figures
{
    public abstract class RotatableShape
    {
        protected int RotationNumber;
        protected int RotationCoefficient;

        protected RotatableShape(int rotationCoefficient)
        {
            this.RotationCoefficient = rotationCoefficient;
            Points = new List<Point>();
        }
        
        public List<Point> Points { get; set; }

        public void Rotate()
        {
            var newPoints = new List<Point>();
            this.RotationNumber += 1;
            for (var i = 0; i < Points.Count; i++)
            {
                Point newPoint;
                if (i != Points.Count - 1)
                {
                    newPoint = GetNewPoint(Points[i], Points[i + 1]);
                }
                else
                {
                    newPoint = GetNewPoint(Points[i], Points[0]);
                }

                newPoints.Add(newPoint);
            }

            Points.Clear();
            Points.AddRange(newPoints);
        }

        private Point GetNewPoint(Point oldPoint, Point oldNextPoint)
        {
            var coefficient = RotateUtils.GetCoefficientRotation(RotationCoefficient, RotationNumber);
            var x = oldPoint.X * (1 - coefficient) + coefficient * oldNextPoint.X;
            var y = oldPoint.Y * (1 - coefficient) + coefficient * oldNextPoint.Y;
            return new Point(x, y);
        }
    }
}