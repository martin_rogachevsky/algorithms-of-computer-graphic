﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using SDL2;

namespace ShapesCrossing
{
    static class Program
    {
        const int SCREEN_WIDTH = 640;
        const int SCREEN_HEIGHT = 480;
        // private SDL.SDL_Point []points = new SDL.SDL_Point[SCREEN_WIDTH * SCREEN_HEIGHT];
        private const int CANVAS_WIDTH = 300;
        private const int CANVAS_HEIGHT = 300;

        private static readonly List<Polygon> Shapes = new List<Polygon>();
        private static readonly Random Rand = new Random();

        static void Main()
        {
            Thread thread = new Thread(() =>
            {
                 SDL.SDL_Init(SDL.SDL_INIT_EVERYTHING);
                IntPtr wnd = SDL.SDL_CreateWindow("ShapesCrossing", 100, 100, SCREEN_WIDTH, SCREEN_HEIGHT,
                    /*SDL.SDL_WindowFlags.SDL_WINDOW_RESIZABLE |*/
                    SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN);
                //  var shape = new Shape();

                var renderer = SDL.SDL_CreateRenderer(wnd, -1, SDL.SDL_RendererFlags.SDL_RENDERER_ACCELERATED);
                var canvas = new SDL.SDL_Rect()
                {
                    h = CANVAS_HEIGHT,
                    w = CANVAS_WIDTH,
                    y = (SCREEN_HEIGHT - CANVAS_HEIGHT) / 2,
                    x = (SCREEN_WIDTH - CANVAS_WIDTH) / 2
                };

                Shapes.Add(new Polygon(new SDL.SDL_Rect() {h = 50, w = 50, x = 200, y = 35}));
                Shapes.Add(new Polygon(
                    new Line[] {new Line(200, 35, 120, 140), new Line(120, 140, 250, 170), new Line(250, 170, 200, 35)},
                    true));

                int underControlShape = 0;
                int moveDir = 1;
                Point mouse = new Point(0, 0);
                int sch = 0;

                bool quit = false;
                while (!quit)
                {
                    foreach (var shape in Shapes)
                    {
                        if (sch % 50 == 0)
                            shape.Rotate(15);
                        if (Shapes.IndexOf(shape) == underControlShape)
                            continue;

                        var x = moveDir;
                        var y = moveDir;
                        if (shape.Lines.Any(l => l.Start.X < 10))
                            moveDir = 1;
                        if (shape.Lines.Any(l => l.Start.X > SCREEN_WIDTH))
                            moveDir = -1;
                        shape.Offset(x, y);
                    }

                    SDL.SDL_SetRenderDrawColor(renderer, 235, 235, 235, 0);
                    SDL.SDL_RenderClear(renderer);

                    SDL.SDL_Event sdlEvent;
                    SDL.SDL_PollEvent(out sdlEvent);
                    switch (sdlEvent.type)
                    {
                        case SDL.SDL_EventType.SDL_QUIT:
                            {
                                quit = true;
                                break;
                            }
                        case SDL.SDL_EventType.SDL_KEYDOWN:
                            {
                                var key = sdlEvent.key;
                                switch (key.keysym.sym)
                                {
                                    case SDL.SDL_Keycode.SDLK_DOWN:
                                        if (underControlShape > 0)
                                            underControlShape--;
                                        break;
                                    case SDL.SDL_Keycode.SDLK_UP:
                                        if (underControlShape < Shapes.Count - 1)
                                            underControlShape++;
                                        break;
                                }
                                break;
                            }
                        case SDL.SDL_EventType.SDL_MOUSEMOTION:
                            {
                                if (mouse.X != 0 && mouse.Y != 0)
                                    Shapes[underControlShape].Offset(sdlEvent.motion.x - mouse.X, sdlEvent.motion.y - mouse.Y);
                                mouse.X = sdlEvent.motion.x;
                                mouse.Y = sdlEvent.motion.y;
                                break;
                            }

                    }

                    sch++;
                    Draw(canvas, renderer);
                    Thread.Sleep(10);
                }
                SDL.SDL_DestroyRenderer(renderer);
                SDL.SDL_DestroyWindow(wnd);
                SDL.SDL_Quit();

            });
            thread.Start();
            thread.Join();
        }

        private static void Draw(SDL.SDL_Rect canvas, IntPtr renderer)
        {
            DrawCanvas(canvas, renderer);
            DrawShapes(canvas, renderer);

            SDL.SDL_RenderPresent(renderer);
        }

        private static void DrawCanvas(SDL.SDL_Rect canvas,IntPtr renderer)
        {
            SDL.SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
            SDL.SDL_RenderDrawRect(renderer, ref canvas);

            SDL.SDL_SetRenderDrawColor(renderer, 220, 220, 220, 0);
            canvas.h = CANVAS_HEIGHT - 2;
            canvas.w = CANVAS_WIDTH - 2;
            canvas.x++;
            canvas.y++;
            SDL.SDL_RenderFillRect(renderer, ref canvas);
        }

        private static void DrawShapes(SDL.SDL_Rect canvas, IntPtr renderer)
        {
            var canvasPoly = new Polygon(canvas);
            var result = new Polygon(false);

            SDL.SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
            foreach (var shape in Shapes.AsEnumerable().Reverse())
            {
                result = new Polygon(ShapesJoiner.Join(shape, result, false).Lines);
                result.Lines.AddRange(shape.Lines);
            }
            ShapesJoiner.Join(canvasPoly, result).Draw(renderer);
           
            result = new Polygon(false);
            var trash = new Polygon(false);
            foreach (var shape in Shapes.AsEnumerable().Reverse())
            {
                trash.Lines.AddRange(ShapesJoiner.Join(shape, result).Lines);
                result.Lines.AddRange(shape.Lines);
            }
            trash.Lines.AddRange(ShapesJoiner.Join(canvasPoly, result, false).Lines);
            trash.Draw(renderer,true);
        }
    }
}
