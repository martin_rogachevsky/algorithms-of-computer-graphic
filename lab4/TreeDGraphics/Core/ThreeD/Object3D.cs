﻿using System;
using System.Collections.Generic;
using System.Linq;
using TreeDGraphics.Core.Drawers;
using TreeDGraphics.Core.TwoD;

namespace TreeDGraphics.Core.ThreeD
{
    public class Object3D
    {
        public readonly List<Triangle3D> Polygons = new List<Triangle3D>();
        public bool NeedToRerender;
        private ProjectionDrawer _drawer;
        private PerspectiveProjectionDrawer _perspectiveProjectionDrawer;

        public Object3D(IEnumerable<Triangle3D> polygons)
        {
            Polygons.AddRange(polygons);
            NeedToRerender = true;
        }

        public void Move(Point3D offset)
        {
            foreach (var polygon in Polygons)
            {
                foreach (var line in polygon.Lines)
                {
                    line.Start = new Point3D(line.Start.X + offset.X, line.Start.Y + offset.Y, line.Start.Z + offset.Z);
                    line.End = new Point3D(line.End.X + offset.X, line.End.Y + offset.Y, line.End.Z + offset.Z);
                }
            }
        }

        public Point3D MoveToCoordinateSystemStart()
        {
            float x = 0;
            float y = 0;
            float z = 0;

            Polygons.ForEach(p => p.Lines.ForEach(l => x = x + l.Start.X + l.End.X));
            Polygons.ForEach(p => p.Lines.ForEach(l => y = y + l.Start.Y + l.End.Y));
            Polygons.ForEach(p => p.Lines.ForEach(l => z = z + l.Start.Z + l.End.Z));

            x = -1 * x / (Polygons.Count * 6);
            y = -1 * y / (Polygons.Count * 6);
            z = -1 * z / (Polygons.Count * 6);
            var offset = new Point3D(x, y, z);
            Move(offset);
            return offset;
        }

        public void Draw(IntPtr renderer, Point3D coordSysPos, bool isLineHidden, bool isLineShown)
        {
            var drawingObject = new Object3D(Polygons);
            drawingObject.Move(coordSysPos);

            if (NeedToRerender)
            {
                var transform = new Dictionary<Triangle3D, Polygon2D>();
                foreach (var poly in drawingObject.Polygons)
                {
                    transform.Add(poly,
                        new Polygon2D(poly.Lines.Select(p =>
                            new Line2D(p.Start.X, p.Start.Z, p.End.X, p.End.Z, p.IsTrash)).ToList(), true));
                }

                _drawer = new ProjectionDrawer(transform,
                    new Triangle3D(new Point3D(0, 0, 1), new Point3D(0, 0, -1), new Point3D(1, 0, 0), null));
            }

            _drawer.Draw(renderer, NeedToRerender, isLineHidden, isLineShown);
            NeedToRerender = false;
            drawingObject.Move(new Point3D((-1) * coordSysPos.X, (-1) * coordSysPos.Y, (-1) * coordSysPos.Z));
        }

        public void DrawInPerspective(IntPtr renderer, float r, int phi, int teta, float d)
        {
            _perspectiveProjectionDrawer = new PerspectiveProjectionDrawer(Polygons, r, d, teta, phi);
            _perspectiveProjectionDrawer.Draw(renderer);
        }
    }
}